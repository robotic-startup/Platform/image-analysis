import pydot
import LoadBatches, Models
from keras.utils import plot_model

modelFN = Models.VGGSegnet_hacked.VGGSegnet

m = modelFN(2, 224, 224)

plot_model(m, "model.png")
