import numpy as np
import matplotlib.pyplot as plt
import os
import datetime
import argparse
from io import BytesIO
import tarfile
import tempfile
from matplotlib.image import imread

from six.moves import urllib
from matplotlib import gridspec
import cv2
ap = argparse.ArgumentParser()

ap.add_argument("--predictions", help="location of predicted annotations")
ap.add_argument("--images", help="location of images")
args = vars(ap.parse_args())

LABEL_NAMES = np.asarray([
    'background', 'Rasen'
])

def vis_segmentation(image, seg_map, image_name, output_vis_dir):
  """Visualizes input image, segmentation map and overlay view."""
  plt.figure(figsize=(18, 10))
  grid_spec = gridspec.GridSpec(1, 2, width_ratios=[1,2])

  plt.subplot(grid_spec[0])
  plt.imshow(image)
  plt.axis('off')
  plt.title('input image')

  plt.subplot(grid_spec[1])
  seg_map.astype(float)

  plt.imshow(seg_map)
  plt.axis('off')
  plt.title('segmentation map')

  # plt.subplot(grid_spec[2])
  # plt.imshow(image)
  # plt.imshow(seg_image, alpha=0.7)
  # plt.axis('off')
  # plt.title('segmentation overlay')
  #
  # unique_labels = np.unique(seg_map)
  # ax = plt.subplot(grid_spec[3])
  # plt.imshow(
  #     FULL_COLOR_MAP[unique_labels].astype(np.uint8), interpolation='nearest')
  # ax.yaxis.tick_right()
  # plt.yticks(range(len(unique_labels)), LABEL_NAMES[unique_labels])
  # plt.xticks([], [])
  # ax.tick_params(width=0.0)
  # plt.grid('off')

  plt.savefig(output_vis_dir + '/' + image_name)
  plt.close()



#read files
ann_source = args['predictions']
im_source = args['images']
dest = ann_source.rsplit('/', 1)[0] + "/visualization " + str(datetime.datetime.now())
print("dest is: " + str(dest))
os.mkdir(dest)
prediction_files = sorted(os.listdir(ann_source))
image_files  = sorted(os.listdir(im_source))

#visualize and save images
for i in range(len(prediction_files)):
  image = imread(ann_source + "/" + image_files[i])
  print("visualizing " , prediction_files[i])
  seg_map = imread(im_source + "/" + prediction_files[i])
  vis_segmentation(image, seg_map, prediction_files[i], dest)

print("visualized " + str(len(prediction_files)) + " files")
