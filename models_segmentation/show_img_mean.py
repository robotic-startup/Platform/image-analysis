import cv2
import os
import numpy as np
from matplotlib import pyplot as plt
import time

img_file_path = "../../dataset_current/dataset_own_v6/images_train/"
width, height = (224,224)

imgs = os.listdir(img_file_path)
means_r = []
means_g = []
means_b = []
min_r, max_r = ([], [])
for i in imgs[:100]:
    path = img_file_path + i
    img = cv2.imread(path, 1)

    means_r.append(np.mean(img[:,:,0]))
    means_g.append(np.mean(img[:,:,1]))
    means_b.append(np.mean(img[:,:,2]))


    #sub_mean
    img = cv2.resize(img, ( width , height ))


    img = img.astype(np.float32)
    img[:,:,0] -= np.mean(img[:,:,0])
    img[:,:,1] -= np.mean(img[:,:,1])
    img[:,:,2] -= np.mean(img[:,:,2])


    min_r.append(np.min(img))
    max_r.append(np.max(img))
    # print(min_r[-1], max_r[-1])
    # print(means_r[-1])

    # print(means_g[-1])
    # print(means_b[-1])
    img = img/255 + 0.5
    
    min_r.append(np.min(img))
    max_r.append(np.max(img))
    print(min_r[-1], max_r[-1])
    plt.imshow(img)
    plt.show()
print(means_r, means_g, means_b)
