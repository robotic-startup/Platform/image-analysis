import argparse
import Models 
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.python.keras.callbacks import TensorBoard, ModelCheckpoint

from time import time
import numpy as np
from tensorflow.python.keras import backend as K
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import cv2
import json
import random
from datetime import datetime
import os
from IPython.display import clear_output

parser = argparse.ArgumentParser()
parser.add_argument("--save_weights_path", type = str, default="weights/"  )

parser.add_argument("--n_classes", type=int , default=30)
parser.add_argument("--input_height", type=int , default = 130  )
parser.add_argument("--input_width", type=int , default = 384 )
parser.add_argument("--experiment_name", type=str, default="debug")
# parser.add_argument("--experiment_name", type=str, required=True)
parser.add_argument("--run_name", type=str, default="")

parser.add_argument("--train_images", type = str, default="../dataset_current/dataset_own_v9.1/images_train/"  )
parser.add_argument("--train_annotations", type = str , default="../dataset_current/dataset_own_v9.1/annotations_train/" )
parser.add_argument("--val_images", type = str , default = "../dataset_current/dataset_own_v9.1/images_val/")
parser.add_argument("--val_annotations", type = str , default = "../dataset_current/dataset_own_v9.1/annotations_val/")

parser.add_argument("--teacher_annotations", type = str , default="../dataset_current/distilled5/images_train/" )
parser.add_argument("--teacher_annotations_val", type = str , default="../dataset_current/distilled5/images_val/" )

parser.add_argument("--epochs", type = int, default = 300 )
parser.add_argument("--train_batch_size", type = int, default = 32 )
parser.add_argument("--val_batch_size", type = int, default = 32 )
parser.add_argument("--val_steps", type = int , default = 5)
parser.add_argument("--train_steps", type = int , default = 30)

parser.add_argument("--optimizer_name", type = str , default = "rmsprop")
parser.add_argument("--loss_name", type = str , default = "categorical_crossentropy")
parser.add_argument("--image_normalization", type = str , default = "sub_mean")

parser.add_argument("--model_name", type = str , default = "mobilenetv2_unet")
parser.add_argument("--initialize_weights", type=str, default="", help="Punch in the model filepath that you want to use as the initial weights")

parser.add_argument("--augment", action="store_true", default=False)
parser.add_argument("--zoom_intensity", type = float, default = 0.2 )
parser.add_argument("--flip_horizontal", action="store_true", default=False)
parser.add_argument("--options", type=str, default="{}")
parser.add_argument("--temp", type=int, default=5)
parser.add_argument("--is_student", action="store_true", default=False)


args = parser.parse_args()

n_classes = args.n_classes

input_height = 128
input_width =128

output_height = 128
output_width = 128


# wandb
# import wandb
# from wandb.keras import WandbCallback
# wandb.init(project="cityscapes", name=args.run_name, group =args.experiment_name)
# wandb.config.update(args)


# dataset, info = tfds.load('oxford_iiit_pet:3.*.*', with_info=True)
dataset, info = tfds.load('cityscapes/semantic_segmentation', data_dir = "~/image-analysis/dataset_current/cityscapes", with_info=True)

modelFns = { 
  'mobile_net_v2':Models.Mobile_Net_v2.MobileNet,
  'mobilenetv2_unet':Models.pix2pix.unet_model,
}

modelFN = modelFns[ args.model_name ]


def normalize(input_image):
    input_image = tf.cast(input_image, tf.float32) / 255.0
    return input_image

@tf.function
def load_image_train(datapoint):
    input_image = tf.image.resize(datapoint['image_left'], (input_height, input_width))
    input_mask = tf.image.resize(datapoint['segmentation_label'], (output_height, output_width))
    # input_image = tf.image.resize(datapoint['image'], (input_height, input_width))
    # input_mask = tf.image.resize(datapoint['segmentation_mask'], (output_height, output_width))

    if tf.random.uniform(()) > 0.5:
        input_image = tf.image.flip_left_right(input_image)
        input_mask = tf.image.flip_left_right(input_mask)


    input_image = normalize(input_image)
    input_mask = tf.dtypes.cast(input_mask, tf.uint8)

    input_mask = tf.one_hot(input_mask, n_classes)
    input_mask = tf.reshape(input_mask, (output_height, output_width, n_classes))


    return input_image, input_mask

def load_image_test(datapoint):
    input_image = tf.image.resize(datapoint['image_left'], (input_height, input_width))
    input_mask = tf.image.resize(datapoint['segmentation_label'], (output_height, output_width))
    input_mask = tf.dtypes.cast(input_mask, tf.uint8)
    

    input_image = normalize(input_image)
    input_mask = tf.one_hot(input_mask, n_classes)
    input_mask = tf.reshape(input_mask, (output_height, output_width, n_classes))

    return input_image, input_mask

TRAIN_LENGTH = info.splits['train'].num_examples
BATCH_SIZE = 32
BUFFER_SIZE = 100
STEPS_PER_EPOCH = TRAIN_LENGTH // BATCH_SIZE

mirrored_strategy = tf.distribute.MirroredStrategy()
with mirrored_strategy.scope():
  train = dataset['train'].map(load_image_train, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  test = dataset['test'].map(load_image_test, num_parallel_calls = tf.data.experimental.AUTOTUNE)

  # train_dataset = train.shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()
  train_dataset = train.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()
  # train_dataset = train.shuffle(BUFFER_SIZE).batch().repeat()
  train_dataset = train_dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
  test_dataset = test.batch(BATCH_SIZE)



  def display(display_list):
    plt.figure(figsize=(15, 15))

    title = ['Input Image', 'True Mask', 'Predicted Mask']

    for i in range(len(display_list)):
      plt.subplot(1, len(display_list), i+1)
      plt.title(title[i])

      label = display_list[1].numpy()
      label = np.argmax(label, axis=-1)
      img = display_list[0].numpy()

      if(i==0):
        plt.imshow(img)
      elif(i==1):
        plt.imshow(label)
      else:
        print("not supported yet")
      plt.axis('off')
    plt.show()
    plt.savefig("testtmp.png")


  # for image, mask in train.take(1):
  #   sample_image, sample_mask = image, mask
  # display([sample_image, sample_mask])


  model = modelFN(n_classes)
  model.compile(optimizer=args.optimizer_name, loss='categorical_crossentropy',
                metrics=[tf.keras.metrics.MeanIoU(num_classes=n_classes)])

  # tf.keras.utils.plot_model(model, to_file="model.png", show_shapes=True)

  def create_mask(pred_mask):
    pred_mask = tf.argmax(pred_mask, axis=-1)
    pred_mask = pred_mask[..., tf.newaxis]
    return pred_mask[0]

  def show_predictions(dataset=None, num=1):
    if dataset:
      for image, mask in dataset.take(num):
        pred_mask = model.predict(image)
        display([image[0], mask[0], create_mask(pred_mask)])
    else:
      display([sample_image, sample_mask,
              create_mask(model.predict(sample_image[tf.newaxis, ...]))])


  class DisplayCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
      clear_output(wait=True)
      show_predictions()
      print ('\nSample Prediction after epoch {}\n'.format(epoch+1))

  filepath="weights-improvement-{epoch:02d}-{val_loss:.4f}-{val_mean_iou_score:04f}.hdf5"
  # filepath = os.path.join(wandb.run.dir, "model-" + filepath + ".h5")

  checkpoint = ModelCheckpoint(filepath, monitor='val_mean_iou', save_best_only=False, mode="max")


  # callbacks_list = [checkpoint, WandbCallback()]
  # callbacks_list = [ WandbCallback()]
  callbacks_list = [checkpoint]

  VAL_SUBSPLITS = 5
  VALIDATION_STEPS = info.splits['test'].num_examples//BATCH_SIZE//VAL_SUBSPLITS

  model_history = model.fit(train_dataset, epochs=args.epochs,
                          steps_per_epoch=STEPS_PER_EPOCH,
                          validation_steps=args.val_steps,
                          validation_data=test_dataset,
                          callbacks=callbacks_list)