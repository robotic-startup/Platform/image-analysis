import augmentation_functions as aug 
import cv2
import matplotlib.pyplot as plt 
import argparse 
import os
import numpy as np
import matplotlib
import LoadBatches
matplotlib.use('agg')  # Or any other X11 back-end


parser = argparse.ArgumentParser()
parser.add_argument("--train_images", type = str, default="../dataset_current/dataset_own_v9.1/images_train/"  )
parser.add_argument("--train_annotations", type = str , default="../dataset_current/dataset_own_v9.1/annotations_train/" )
output_height = 16
output_width = 48
args = parser.parse_args()

images = os.listdir(args.train_images)
images.sort()
annotations = os.listdir(args.train_annotations)
annotations.sort()

out_path = "augment_test_images/"
if not os.path.exists(out_path):
    os.makedirs(out_path)

augmentations = [aug.random_zoom, aug.flip_axis]
intensities=[0.2, -1]

print([str(i) for i in augmentations])
print("intensities: ", intensities)

for index in range(10):
    im = args.train_images + "/" + images[index]
    seg_gt = args.train_annotations + "/" + annotations[index]

    tempX = LoadBatches.getImageArr(im , 384 , 130, imgNorm = "sub_mean" )
    tempY_gt = LoadBatches.getSegmentationArr(seg_gt , 2 , output_width=output_width , output_height=output_height , annotation_filter = "oldschool") 

    
    p = np.random.random(size=len(augmentations))
    s = np.random.randint(0,999999, size=len(augmentations))

    for i in range(len(augmentations)):

        tempX_new = augmentations[i](tempX, intensity=intensities[i], seed=s[i])
        tempY_gt_new = augmentations[i](np.reshape(tempY_gt,(output_height, output_width ,2)), intensity=intensities[i], seed=s[i])[:,:,0]
        # tempY_gt = np.reshape(tempY_gt,(output_width*output_height,2))
    tempY_gt = np.reshape(tempY_gt, (output_height, output_width, 2))[:,:,0]


    plt.imshow(tempY_gt)
    plt.savefig(out_path + str(index) + "ann.png")

    plt.imshow( tempX_new[:,:,0])
    plt.savefig(out_path +str(index) + "img_new.png")

    plt.imshow(tempY_gt_new)
    plt.savefig(out_path + str(index) +"ann_new.png")

    plt.imshow(tempX[:,:,0])
    plt.savefig(out_path +str(index) + "img.png")


    print("saved image" + str(index))