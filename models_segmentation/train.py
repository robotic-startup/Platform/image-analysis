import argparse
import Models 
import tensorflow as tf
import metrics_and_losses
import LoadBatches


from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.models import Model 
from tensorflow.python.keras.layers import Lambda, Concatenate, Activation
from tensorflow.python.keras.callbacks import TensorBoard, ModelCheckpoint

from time import time
import numpy as np
from tensorflow.python.keras import backend as K
import matplotlib.pyplot as plt
import cv2
import json
import random
import augmentation_functions as aug


import loadDataset


#print "this script currently runs with python2"

from datetime import datetime
import os

parser = argparse.ArgumentParser()
parser.add_argument("--save_weights_path", type = str, default="weights/"  )

parser.add_argument("--n_classes", type=int , default=2)
parser.add_argument("--input_height", type=int , default = 130  )
parser.add_argument("--input_width", type=int , default = 384 )
parser.add_argument("--experiment_name", type=str, required=True)
parser.add_argument("--run_name", type=str, default="")
parser.add_argument("--annotation_filter", type=str, default="oldschool", help="values can be smoothed, dilated or oldschool")

parser.add_argument("--teacher_annotations", type = str , default="../dataset_current/distilled5/images_train/" )
parser.add_argument("--teacher_annotations_val", type = str , default="../dataset_current/distilled5/images_val/" )
parser.add_argument("--train_images", type = str, default="../dataset_current/dataset_own_v9.1/images_train/"  )
# parser.add_argument("--train_images", type = str, default="../dataset_current/dataset_own_v9.1_reordered/annotations_train/"  )
parser.add_argument("--train_annotations", type = str , default="../dataset_current/dataset_own_v9.1/annotations_train/" )
parser.add_argument("--val_images", type = str , default = "../dataset_current/dataset_own_v9.1/images_val/")
# parser.add_argument("--val_images", type = str , default = "../dataset_current/dataset_own_v9.1_reordered/annotations_val/")
parser.add_argument("--val_annotations", type = str , default = "../dataset_current/dataset_own_v9.1/annotations_val/")

parser.add_argument("--epochs", type = int, default = 300 )
parser.add_argument("--train_batch_size", type = int, default = 32 )
parser.add_argument("--val_batch_size", type = int, default = 32 )

parser.add_argument("--model_name", type = str , default = "vgg_segnet_hacked_channelslast")
parser.add_argument("--initialize_weights", type=str, default="", help="Punch in the model filepath that you want to use as the initial weights")
parser.add_argument("--val_steps", type = int , default = 20)
parser.add_argument("--train_steps", type = int , default = 108)
parser.add_argument("--optimizer_name", type = str , default = "rmsprop")
parser.add_argument("--loss_name", type = str , default = "categorical_crossentropy")
parser.add_argument("--image_normalization", type = str , default = "sub_mean")
parser.add_argument("--augment", action="store_true", default=False)
parser.add_argument("--zoom_intensity", type = float, default = 0.2 )
parser.add_argument("--flip_horizontal", action="store_true", default=False)

parser.add_argument("--options", type=str, default="{}")
parser.add_argument("--temp", type=int, default=5)
parser.add_argument("--is_student", action="store_true", default=False)

args = parser.parse_args()
# #wandb
# import wandb
# from wandb.keras import WandbCallback
# wandb.init(project="lawnrobot", name=args.run_name, group =args.experiment_name)
# wandb.config.update(args)



modelFns = { 
 
  'vgg_segnet_hacked_channelslast':Models.VGGSegnet_hacked_channelslast.VGGSegnet,
  'segnet_dropout':Models.segnet_dropout.SegmentationModel,
  'mobile_net_v1':Models.Mobile_Net_v1.MobileNet,
  'mobile_net_v1_small': Models.Mobile_Net_v1_small.MobileNet,
  'mobile_net_v2':Models.Mobile_Net_v2.MobileNet,
  'mobile_net_v2_deeplab':Models.Mobile_Net_v2_deeplab.MobileNet,
  'icnet':Models.ICNet.ICNet,
  'shufflenet':Models.ShuffleNet.ShuffleNet,
  'xception':Models.Xception_deeplab.Xception,
  'prof_xception':Models.Prof_Xception.Xception,
  'xception_10layer_output':Models.Xception_10layer_output.Xception,
}

modelFN = modelFns[ args.model_name ]


if(args.is_student):

  metrics = [metrics_and_losses.iou_score_student_hard, 
            metrics_and_losses.iou_score_student_soft, 
            metrics_and_losses.distilled_loss_both(args.temp), 
            metrics_and_losses.distilled_loss_teacher(args.temp), 
            metrics_and_losses.distilled_loss_gt(),]

  loss_name = metrics_and_losses.distilled_loss_both(args.temp)

  custom_objects = {
            "iou_score_student_hard" : metrics_and_losses.iou_score_student_hard, 
            "iou_score_student_soft" : metrics_and_losses.iou_score_student_soft, 
            "loss_both" : metrics_and_losses.distilled_loss_both(args.temp), 
            "loss_teacher" : metrics_and_losses.distilled_loss_teacher(args.temp), 
            "loss_gt" : metrics_and_losses.distilled_loss_gt()}
      
else:
  loss_name = "categorical_crossentropy"
  metrics = [metrics_and_losses.iou_score, metrics_and_losses.iou_score_student_hard] 
  # metrics = ['acc']
  custom_objects = {"iou_score" : metrics_and_losses.iou_score,  
            "iou_score_student_hard" : metrics_and_losses.iou_score_student_hard,}
  # custom_objects = {}

if(args.loss_name not in locals().keys()):
    loss_fn = args.loss_name
else:
    loss_fn = locals()[args.loss_name]



mirrored_strategy = tf.distribute.MirroredStrategy()
with mirrored_strategy.scope():

  if(args.initialize_weights==""):
    m = modelFN(args.n_classes, input_height=args.input_height, input_width=args.input_width, **(json.loads(args.options)))

  else:
    m = load_model(args.initialize_weights, custom_objects=custom_objects)
    m.outputHeight = 16
    m.outputWidth = 48
    # optimizer = tf.keras.optimizers.RMSprop(learning_rate=0.001)
    m.compile(optimizer = args.optimizer_name, metrics=metrics, loss = loss_fn)
    # m.compile(optimizer = args.optimizer_name, metrics=metrics, loss = loss_fn)

  if (args.is_student):
    logits = m.layers[-2].output
    probabilities = Activation('softmax', name="activation_normal")(logits)

    logits_T = Lambda(lambda x, temp: x / temp, arguments={'temp':args.temp})((logits))
    probabilities_T = Activation('softmax', name="activation_temp")(logits_T)

    o = Concatenate()([probabilities,probabilities_T])
    mnew = Model(m.input, o)
    mnew.outputWidth =  m.outputWidth
    mnew.outputHeight = m.outputHeight
    m = mnew

    print(mnew.layers[-1].output, " is output")
  # optimizer = tf.keras.optimizers.RMSprop(learning_rate=0.001)
  m.compile(loss=loss_fn,
            # optimizer= optimizer ,
            optimizer= args.optimizer_name ,
            metrics=metrics)

  ''' 
      The ranges of intensity values that would make sense:
      zoom: 0 - 0.25
      shear: 0 - 30
      rotation: 0 - 30
      brightness: 0 - 0.3 
      hue: 0 - 0.3
      saturation: 0 - 0.3

  '''

  if (args.augment):
    # augmentations=[aug.random_zoom, aug.random_shear, aug.random_rotation, aug.flip_axis]

    if(args.flip_horizontal):
      augmentations=[aug.random_zoom, aug.flip_axis]
      intensities = [args.zoom_intensity, -1]
    else:
      augmentations=[aug.random_zoom]
      intensities = [args.zoom_intensity]

  else:
    augmentations=[]
    intensities=[]

  # train_generator = LoadBatches.distillationImageSegmentationGenerator(
  #         images_path = args.train_images , 
  #         gt_segs_path = args.train_annotations , 
  #         batch_size = args.train_batch_size,  
  #         n_classes = args.n_classes, 
  #         input_height = args.input_height , input_width =  args.input_width , 
  #         output_height = m.outputHeight , output_width = m.outputWidth, 
  #         imgNorm = args.image_normalization,
  #         augmentations = augmentations, 
  #         intensities = intensities, 
  #         annotation_filter = args.annotation_filter,
  #         teacher_segs_path = args.teacher_annotations, 
  #         is_student=args.is_student,
  #         train_steps=args.train_steps)
  datasetBuilder = True
  if(not datasetBuilder):
    validation_generator = LoadBatches.distillationImageSegmentationGenerator(
            images_path = args.val_images , 
            gt_segs_path = args.val_annotations,
            batch_size = args.val_batch_size, 
            n_classes = args.n_classes , 
            input_height = args.input_height , input_width = args.input_width, 
            output_height = m.outputHeight , output_width = m.outputWidth,
            imgNorm = args.image_normalization, 
            intensities = [], 
            augmentations = [], 
            annotation_filter = args.annotation_filter,
            teacher_segs_path = args.teacher_annotations_val,
            is_student= args.is_student,
            train_steps=args.val_steps)
    train_dataset, val_dataset = loadDataset.load_dataset(
            train_images_path = args.train_images , 
            val_images_path = args.val_images , 
            BATCH_SIZE = args.train_batch_size,  
            nClasses = args.n_classes, 
            input_height = args.input_height , input_width =  args.input_width , 
            output_height = m.outputHeight , output_width = m.outputWidth, 
            imgNorm = args.image_normalization,
            augmentations = augmentations, 
            intensities = intensities, 
            annotation_filter = args.annotation_filter,
            teacher_segs_path = args.teacher_annotations, 
            is_student=args.is_student)

  if(datasetBuilder):

    # tfds_object, info  = tfds.load(name="mnist", data_dir="../dataset_current/cityscapes", with_info=True)
    dataset, info = tfds.load('oxford_iiit_pet:3.*.*', with_info=True)

    train_dataset, val_dataset = tfds_object['train'], tfds_object['test']
    train_dataset = 

    def preprocess_image(image):
      image = tf.image.resize(input_height, input_width)
      return image
    def preprocess_label(label):
      label = tf.image.resize(output_height, output_width)
      label = tf.reshape((output_height * output_width, -1))
      return label
    def normalize(input_image, input_mask):
      input_image = tf.cast(input_image, tf.float32) / 255.0
      input_mask -= 1
      return input_image, input_mask

    @tf.function
    def load_image_train(datapoint):
      input_image = tf.image.resize(datapoint['image'], (input_height, input_width))
      input_mask = tf.image.resize(datapoint['segmentation_mask'], (output_height, output_width))
      input_mask = tf.reshape((output_height * output_width, -1))
  
      if tf.random.uniform(()) > 0.5:
        input_image = tf.image.flip_left_right(input_image)
        input_mask = tf.image.flip_left_right(input_mask)

      input_image, input_mask = normalize(input_image, input_mask)

      return input_image, input_mask

    def load_image_test(datapoint):
      input_image = tf.image.resize(datapoint['image'], (input_height, input_width))
      input_mask = tf.image.resize(datapoint['segmentation_mask'], (output_height, output_width))
      input_mask = tf.reshape((output_height * output_width, -1))

      input_image, input_mask = normalize(input_image, input_mask)

      return input_image, input_mask

    TRAIN_LENGTH = info.splits['train'].num_examples
    BATCH_SIZE = args.train_batch_size
    BUFFER_SIZE = 1000
    STEPS_PER_EPOCH = TRAIN_LENGTH // BATCH_SIZE

    train = dataset['train'].map(load_image_train, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    test = dataset['test'].map(load_image_test)

    train_dataset = train.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()
    train_dataset = train_dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    test_dataset = test.batch(BATCH_SIZE)

   

  
  weights_path_name =  args.save_weights_path +"/" + args.experiment_name + '/' + args.model_name


  # tbcallback = TensorBoard(log_dir="logs/" + args.experiment_name + "/" + unique_folder_name,
  #                         histogram_freq=0, write_graph=True, write_images=True)

  filepath=weights_path_name + "weights-improvement-{epoch:02d}-{val_loss:.4f}-{val_iou_score_student_hard:04f}.hdf5"

  # checkpoint = ModelCheckpoint(filepath, monitor='val_acc', save_best_only=False, mode="max")
  filepath = os.path.join(wandb.run.dir, "model-" + filepath + ".h5")
  checkpoint = ModelCheckpoint(filepath, monitor='val_iou_score_student_hard', verbose=1, save_best_only=True, mode='max')


  callbacks_list = [checkpoint, WandbCallback()]
  # callbacks_list = [checkpoint, tbcallback]

  # #ceck dataset
  # im_tempfiles_train = os.listdir(args.train_images)
  # an_tempfiles_train = os.listdir(args.train_annotations)
  # assert(len(im_tempfiles_train) == len(an_tempfiles_train) and len(im_tempfiles_train)> 0)

  #only evaluate if epochs==0
  if(args.epochs==0):
    # history = m.evaluate_generator(validation_generator, 
    if(not datasetBuilder):
      history = m.evaluate(x = val_dataset, 
                                    steps=args.val_steps, 
                                    verbose=1, callbacks=callbacks_list)

    results = list(zip(m.metrics_names, history))
    for r in results:
      print(r)

  else:
    try:
      if(datasetBuilder):
        history = m.fit( x=train_images, y=train_labels
              steps_per_epoch=args.train_steps,
              validation_data=val_dataset, 
              validation_steps=args.val_steps ,  
              epochs=args.epochs , 
              callbacks=callbacks_list, use_multiprocessing=False)
      else:

        history = m.fit( x=train_dataset, 
              steps_per_epoch=args.train_steps,
        # history = m.fit_generator( train_generator , args.train_steps, 
              validation_data=val_dataset, 
              # validation_data=validation_generator , 
              validation_steps=args.val_steps ,  
              epochs=args.epochs , 
              callbacks=callbacks_list, use_multiprocessing=False)

    except StopIteration:
      print("Training could not start, check your file path!\n")
    except KeyboardInterrupt:
        if len(os.listdir(weights_path_name))==1:
            os.unlink(weights_path_name + "/model_summary.txt")
            os.rmdir(weights_path_name)
        print ("\n Ouch!")