#! /bin/bash
#SBATCH --output=training_console/cuda_debug.txt
module load anaconda/3

export PATH=/home/ozastrow/cuda10.1/bin:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda10.1/lib64/


srun python3 train.py --train_steps=110 --epochs=300 \
--experiment_name="debug" \
--val_steps=20 --optimizer_name="rmsprop" \
--run_name="multi gpu with segnet loadbatches"
#flip horizontal is not set