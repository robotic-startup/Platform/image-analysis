#! /bin/bash
#SBATCH --output=training_console/shufflenet_rmsprop.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=97 --epochs=300 --model_name="shufflenet" --experiment_name="shufflenet_rmsprop" --optimizer_name="rmsprop" --options='{"decoder_size":1, "num_shuffle_units":[2,5], "scale_factor":0.5, "bottleneck_ratio":1.0,"groups":2}'
