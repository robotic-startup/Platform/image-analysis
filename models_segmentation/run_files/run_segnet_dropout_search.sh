#! /bin/bash
#SBATCH --output=training_console/segnet_dropout_search.txt
module load anaconda/3

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=110 --epochs=80 \
--model_name=segnet_dropout \
--experiment_name="dropout_sweep_v2" \
--val_steps=20 --optimizer_name="rmsprop" \
--options='{"dropout_rate_a":0.3, "dropout_rate_b":0.15, "dropout_rate_c":0.05}' \
--initialize_weights="selected_trained_models/segnet_dropout_138_0.902.h5"

srun python3 train.py --train_steps=110 --epochs=80 \
--model_name=segnet_dropout \
--experiment_name="dropout_sweep_v2" \
--val_steps=20 --optimizer_name="rmsprop" \
--options='{"dropout_rate_a":0.05, "dropout_rate_b":0.15, "dropout_rate_c":0.3}' \
--initialize_weights="selected_trained_models/segnet_dropout_138_0.902.h5"

srun python3 train.py --train_steps=110 --epochs=80 \
--model_name=segnet_dropout \
--experiment_name="dropout_sweep_v2" \
--val_steps=20 --optimizer_name="rmsprop" \
--options='{"dropout_rate_a":0.1, "dropout_rate_b":0.3, "dropout_rate_c":0.1}' \
--initialize_weights="selected_trained_models/segnet_dropout_138_0.902.h5"
