#! /bin/bash
#SBATCH --output=training_console/temperature5_vgg.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --model_name=vgg_segnet_hacked_channelslast --experiment_name="student_vgg" --annotation_filter="oldschool" --temp=5 --is_student
