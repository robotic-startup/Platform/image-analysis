#! /bin/bash
#SBATCH --output=training_console/pruning_l1.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 pruning.py --model_type='vgg' --epochs=60 --pruning_iterations=6 --method="l1" --plot_val_ious
