#! /bin/bash
#SBATCH --output=training_console/temperature5.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=97 --epochs=300 --model_name="vgg_segnet_hacked_channelslast" --experiment_name="vgg_distilled_both" --temp=5 --is_student
