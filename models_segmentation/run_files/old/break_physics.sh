#! /bin/bash
#SBATCH --output=training_console/break_pysics_teacheronly.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=400 --val_steps=72 --epochs=50 --train_batch_size=8 --val_batch_size=8 --model_name=xception --experiment_name="breaking_physics_onlyteacher" --options='{"dropout_rate":0.8, "OS":16, "mid_repeat":8}' --annotation_filter="oldschool" --teacher_annotations="../dataset_current/distilled5/images_train/" --teacher_annotations_val="../dataset_current/distilled5/images_val/" --temp=2 --is_student
