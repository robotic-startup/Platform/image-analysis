#! /bin/bash
#SBATCH --output=training_console/benchmark.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 armand_benchmark.py --model_file="selected_trained_models/weights-improvement-380-0.9496-0.0668.hdf5"
