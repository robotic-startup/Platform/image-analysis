#! /bin/bash
#SBATCH --output=training_console/temperature1.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=97 --epochs=300 --model_name=shufflenet --experiment_name="student_shufflenet" --options='{"decoder_size":2, "num_shuffle_units":[1,3,1], "bottleneck_ratio":1.0,"scale_factor":0.5}' --annotation_filter="oldschool" --teacher_annotations="../dataset_current/distilled1/images_train/" --temp=1

