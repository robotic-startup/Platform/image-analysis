#! /bin/bash
#SBATCH --output=training_console/convert_imgs_temp2.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 convert_predictions_to_image.py --model_file=weights/train_teacher_xception_temp2/prof_xception__id4118/weights-improvement-268-0.1174.hdf5 --output_folder=../dataset_current/distilled3/
