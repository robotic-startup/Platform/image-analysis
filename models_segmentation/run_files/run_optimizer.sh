#! /bin/bash
#SBATCH --output=training_console/optimizer_comparison.txt
module load anaconda/3

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=110 --epochs=250 \
--experiment_name="optimizer_comp" \
--val_steps=20 --optimizer_name="rmsprop" \
--augment \
--run_name="rmsprop"


srun python3 train.py --train_steps=110 --epochs=250 \
--experiment_name="optimizer_comp" \
--val_steps=20 --optimizer_name="nadam" \
--augment \
--run_name="nadam"


srun python3 train.py --train_steps=110 --epochs=250 \
--experiment_name="optimizer_comp" \
--val_steps=20 --optimizer_name="adam" \
--augment \
--run_name="adam"

