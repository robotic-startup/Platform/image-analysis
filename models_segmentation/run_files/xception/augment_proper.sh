#! /bin/bash
#SBATCH --output=training_console/augmentations_fixed.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=400 --val_steps=72 --epochs=50 --train_batch_size=8 --val_batch_size=8 --epochs=300 --model_name=xception --experiment_name="proper_augmentations" --options='{"dropout_rate":0.55, "OS":16, "mid_repeat":8}' --annotation_filter="oldschool" --augment

