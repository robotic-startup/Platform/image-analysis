#! /bin/bash
#SBATCH --output=training_console/train_teacher2.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=500 --val_steps=80 --epochs=400 --train_batch_size=8 --val_batch_size=8 --model_name=xception --experiment_name="xception_9.1" --options='{"dropout_rate":0.55, "OS":16, "mid_repeat":8, "temp":2}' --annotation_filter="oldschool"
