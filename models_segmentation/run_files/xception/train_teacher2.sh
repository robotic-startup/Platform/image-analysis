#! /bin/bash
#SBATCH --output=training_console/train_teacher2.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=400 --val_steps=72 --epochs=400 --train_batch_size=8 --val_batch_size=8 --model_name=prof_xception --experiment_name="train_teacher_xception_temp2" --options='{"dropout_rate":0.55, "OS":16, "mid_repeat":8, "temp":2}' --annotation_filter="oldschool" --temp=2
