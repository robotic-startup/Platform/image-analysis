#! /bin/bash
#SBATCH --output=training_console/segnet_dropout.txt
module load anaconda/3

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64

srun python3 train.py --train_steps=110 --epochs=400 --model_name=segnet_dropout --experiment_name="dropout" --annotation_filter="oldschool" --val_steps=20 --optimizer_name="rmsprop"
