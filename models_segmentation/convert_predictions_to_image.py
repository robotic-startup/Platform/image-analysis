import argparse
import time
import os
import numpy as np
import LoadBatches
from scipy.misc import toimage
import tensorflow
from tensorflow.python.keras.models import load_model
import metrics_and_losses
from tqdm import tqdm

parser = argparse.ArgumentParser()

parser.add_argument("--model_file", type = str, 
		default = "selected_trained_models/xception/dilated_unweighted_297-0.9491-0.0612.hdf5",
                    help="the file can be hdf5  or h5, tflite not supported yet")
parser.add_argument("--images_train", type = str , default = "../dataset_current/tempv9/obstacle/")
parser.add_argument("--images_val", type = str , default = "../dataset_current/tempv9/obstacle/")
parser.add_argument("--output_folder", type = str , default = "../dataset_current/autopredict")
parser.add_argument("--is_for_teacher" , action='store_true', help="if set the output will be soft and not argmaxed")


#set to True if val set should also be converted, default is false
convert_second_dir_also = False

#parser.add_argument("--iou_type", type=str, default=)

args = parser.parse_args()

m = load_model(args.model_file,custom_objects={'iou_score':metrics_and_losses.iou_score, 'iou_score_student_hard':metrics_and_losses.iou_score_student_hard})

last_layer_shape = m.layers[-3].output_shape
first_layer_shape = m.layers[0].output_shape[0]
print(first_layer_shape)
input_width = first_layer_shape[2]
input_height = first_layer_shape[1]
output_width = last_layer_shape[2]
output_height = last_layer_shape[1]


# #add temperature
# logits = m.layers[-2].output
# logits_T = Lambda(lambda x, temp: x / temp, arguments={'temp':args.temp})((logits))
# probabilities_T = Activation('softmax', name="activation_temp")(logits_T)
# mnew = Model(m.input, logits_T)
# mnew.outputWidth =  m.outputWidth
# mnew.outputHeight = m.outputHeight
# m = mnew


train_files= os.listdir(args.images_train)
if(convert_second_dir_also):
	val_files= os.listdir(args.images_val)

dirname = args.output_folder+'/'

print("Loaded model, starting prediction")

if not os.path.exists(dirname+'images_train/'):
	os.makedirs(dirname+'images_train/')
if(convert_second_dir_also):
	if not os.path.exists(dirname+'images_val/'):
		os.makedirs(dirname+'images_val/')

for img in tqdm(train_files):

	X = LoadBatches.getImageArr(args.images_train + img , input_width  , input_height)
	pr = m.predict(np.array([X]))[0]
	if(args.is_for_teacher):
		pr = pr.reshape((output_height ,  output_width, 2))[:,:,0]
	else:
		pr = pr.reshape((output_height ,  output_width, 2)).argmax(axis=-1)
		
	
	#pr = np.stack([pr,pr,pr], axis =2)
	im = toimage(pr, mode="L")
	im.save(dirname+'images_train/'+img[:-3]+"png")

print("Train images predicted")
if(convert_second_dir_also):
	for img in tqdm(val_files):

		X = LoadBatches.getImageArr(args.images_val + img , input_width  , input_height)
		pr = m.predict(np.array([X]))[0]
		if(args.is_for_teacher):
			pr = pr.reshape((output_height ,  output_width, 2))[:,:,0]
		else:
			pr = pr.reshape((output_height ,  output_width, 2)).argmax(axis=-1)
		
	
		#pr = np.stack([pr,pr,pr], axis =2)
		im = toimage(pr, mode="L")
		im.save(dirname+'images_val/'+img[:-3]+"png")

print("Validation images predicted")
print("Xception shall return to his slumber...")