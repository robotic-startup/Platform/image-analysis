#! /bin/bash
#SBATCH --output=training_console/shufflenet0.txt

module load cuda/9.0 anaconda/3
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/ozastrow/cuda/lib64
srun python3 convert_predictions_to_image.py
