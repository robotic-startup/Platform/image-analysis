from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
# import tensorflow.compat.v2 as tf
import tensorflow_datasets as tfds


# Construct a tf.data.Dataset
tfds_object, info  = tfds.load(name="mnist", data_dir="../dataset_current/tfds", with_info=True)
train_ds, val_ds = tfds_object['train'], tfds_object['test']

# Build your input pipeline
train_ds = train_ds.shuffle(1024).batch(32).prefetch(tf.data.experimental.AUTOTUNE)
val_ds = val_ds.shuffle(1024).batch(32).prefetch(tf.data.experimental.AUTOTUNE)
for features in train_ds.take(1):
  image, label = features["image"], features["label"]

print(info)