import matplotlib.pyplot as plt
import numpy as np
import cv2
import math
from matplotlib import path
import time



class Ray:
    def __init__(self, angle, angle_width, origin, length,seg_map):
        #the shape of the robot is needed so that the rays don't see the robot as obstacle
        #the area of the robot can be sourced from segmap, by removing all points that are marked as obstacle from the origin onwards,
        #until the points are not labeled as obstacle (because they are further away from the robot) 

        self.origin = origin
        self.length = length
        self.angle_width = angle_width
        self.last_distance = length
        self.resolution = seg_map.shape[0]*2
        p1 = origin
        p2 = [0.0, 0.0]
        p3 = [0.0, 0.0]
        self.angle = angle  * math.pi*2 /360
        self.angle_width = angle_width * math.pi*2 /360


        p2[1] = math.cos(self.angle) * self.length + p1[1]
        p2[0] = math.sin(self.angle) * self.length + p1[0]

        p3[1] = math.cos(self.angle_width) * self.length + p1[1]
        p3[0] = math.sin(self.angle_width) * self.length + p1[0]

        self.point_list = []
        
        x, y = np.meshgrid(np.arange(seg_map.shape[1]), np.arange(seg_map.shape[0])) # make a canvas with coordinates
        x, y = x.flatten(), y.flatten()
        empty_map = np.vstack((x,y)).T 

        ray_polygon = path.Path([origin, p2,p3])
        
        
        mask_map = ray_polygon.contains_points(empty_map)
        mask_map = mask_map.reshape(seg_map.shape)
        mask_map = np.rot90(mask_map)

        area_points_list = np.where(mask_map)
        area_points_list = np.vstack((area_points_list[0], area_points_list[1], np.zeros(area_points_list[0].shape))).T
    
        
        for i in range(len(area_points_list)):
            area_points_list[i] = list(area_points_list[i])
            area_points_list[i][0] = area_points_list[i][0]
            area_points_list[i][1] = area_points_list[i][1]


        #calculate distance between each point and the origin and then sort according to pixel distance from origin
        def sortKeyThird(val):
            return val[2]

        area_points_list[:, 2] = np.sqrt((area_points_list[:,0]-origin[0])**2 + (area_points_list[:,1] - origin[1])**2)
        area_points_list = area_points_list.astype(int)
        area_points_list = list(area_points_list)
        area_points_list.sort(key = sortKeyThird)
        self.point_list = area_points_list


        #remove those points from list that are within the mower shape
        index_to_remove = 0
        
        # for i in range(len(self.point_list)):
        #     x = self.point_list[i][0]
        #     y = self.point_list[i][1]
            
        #     #if pixel value == 0 this is a pixel classified as obstacle in seg_map
        #     if(x<seg_map.shape[1] and y<seg_map.shape[0]):
        #         if(seg_map[y,x] == 0):
        #             index_to_remove = i
        #         else:
        #             break

        # self.point_list = self.point_list[index_to_remove+1:]
        # area_points_list = area_points_list[index_to_remove+1:]
        i = 0

        while(i<len(self.point_list)):

            x = self.point_list[i][0]
            y = self.point_list[i][1]
            not_outofbounds = x<seg_map.shape[1] and y<seg_map.shape[0]
            is_robotmap_pixel = seg_map[y,x]== 0
            if( not_outofbounds and is_robotmap_pixel):
                self.point_list.pop(i)
                i-=1
            i+=1

        #take start point and recalculate distances from here
        if(len(self.point_list)>0):
            x0 = self.point_list[0][0]
            y0 = self.point_list[0][1]
            area_points_list = np.array(area_points_list).astype(int)

            area_points_list[:, 2] = np.sqrt((area_points_list[:,0]-x0)**2 + (area_points_list[:,1] - y0)**2)
            area_points_list = list(area_points_list)
            self.point_list = area_points_list
            self.length = np.sqrt((self.point_list[0][0]-self.point_list[-1][0])**2 + (self.point_list[0][1] -self.point_list[-1][1])**2)
        else: 
            self.length = 0

        output_height = seg_map.shape[0]
        output_width = seg_map.shape[1]

        #remove out of bounds points from list
        i= 0

        while(i<len(self.point_list)):
            
            if(self.point_list[i][1] >= output_height  or self.point_list[i][1] < 0 or 
            self.point_list[i][0] >= output_width or self.point_list[i][0] < 0):
                self.point_list.pop(i)
                i-=1
            i+=1
        self.np_point_list = np.array(self.point_list)


    def calc_distance(self, seg_map):

        ###vectorized implementation

        distance = self.length    

        #rays could have zero pixels in them and are then completely useless. This is simply catching the error
        # if(len(self.point_list) == 0):
        #     print("empty ray found")
        #     return [0, self.length, self.angle]
        mask = seg_map[self.np_point_list[:,1] , self.np_point_list[:,0]]==0
    
        indexes_obstacles = np.where(mask)

        if(len(indexes_obstacles[0]) == 0):
            distance = self.length
        else:
            first_obstacle_index = indexes_obstacles[0][0]
            distance = self.point_list[first_obstacle_index][2]
            
        
        self.last_distance = distance
        return [distance, self.length, self.angle]

    def draw_line(self, img_plain, seg_map, ray_color):

        # draw this ray for visualization
        
        mask = seg_map[self.np_point_list[:,1] , self.np_point_list[:,0]]==0
        indexes_obstacles = np.where(mask)
        
        if(len(indexes_obstacles[0]) == 0):
            first_obstacle_index = len(self.np_point_list)
        else:
            first_obstacle_index = indexes_obstacles[0][0]

        img_plain[self.np_point_list[:first_obstacle_index][:,1], self.np_point_list[:first_obstacle_index][:, 0]] = ray_color

        
        return img_plain


def create_rays(img):
    ray_list = []
    
    origin = [int(img.shape[1]/2), -10]
    # origin = [int(img.shape[1]/2), int(img.shape[0]/4)]

    #length is a hyperparameter
    length = img.shape[0]*2.1

    angle = 10
    #front ray
    ray_list.append(Ray(-angle, angle, origin, length, img))

    #rays on right side
    for i in range(1,6):
        ray_list.append(Ray(i*angle, (i+1)*angle, origin, length-i*2.2*length/60, img))

    #rays on right side
    for i in range(1,6):
        ray_list.append(Ray(-i*angle, -(i+1)*angle, origin, length-i*2.2*length/60, img))

    return ray_list

def get_distance_vect(ray_list, seg_map):
    #function returns list of n_rays, 3. each list element contains self.distance, self.length, self.angle of the ray
    dist_vect = []
    seg_map_ones = seg_map/255
    for i in range(len(ray_list)):
        dist_vect.append(ray_list[i].calc_distance(seg_map_ones))

    return dist_vect

def visualize_dist_vect(ray_list, img_vis):
    img_plain = np.ones((img_vis.shape[0], img_vis.shape[1], 3))
    small_ray_length = 0.25 * img_vis.shape[0]
    for i in range(len(ray_list)):
        ray_color=[0,255,255]

        if(ray_list[i].length>=small_ray_length):
            if(i%2==0):
                ray_color=[255,255,0]
            else:
                ray_color=[255,0,255]
            
            img_plain = ray_list[i].draw_line(img_plain, img_vis, ray_color)
        


    return img_plain


