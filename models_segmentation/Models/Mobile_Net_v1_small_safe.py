from keras.models import *
from keras.layers import *
from keras import backend as K
from keras.initializers import glorot_normal

import os
file_path = os.path.dirname( os.path.abspath(__file__) )

''' alpha : The width multiplier for the model.
  			The number of input and output channels at each layer are multiplied by this value.
			Default is 1.0, provides a quadratic reduction in complexity.

	depth_multiplier : The resolution multiplier. 
		    The resolution of the input image and the subsequent internal represantations are multiplied by this value.
		    Default is 1.0, provides a quadratic reduction in complexity.

	See the MobileNet paper for more info: https://arxiv.org/pdf/1704.04861.pdf
'''


def MobileNet(n_classes, input_height=800, input_width=1400, **kwargs):

	img_input = Input(shape=(input_height,input_width,3))

	alpha = kwargs["alpha"]
	depth_multiplier = kwargs["depth_multiplier"]
    
	def _conv_block(inputs, filters, alpha=1, kernel=(3, 3), strides=(1, 1)):

	    channel_axis = 1 if K.image_data_format() == 'channels_first' else -1
	    filters = int(filters * alpha)
	    x = ZeroPadding2D(padding=((0, 1), (0, 1)), name='conv1_pad')(inputs)
	    x = Conv2D(filters, kernel, padding='valid', use_bias=False, strides=strides, name='conv1')(x)
	    x = BatchNormalization(axis=channel_axis, name='conv1_bn')(x)
	    return ReLU(6., name='conv1_relu')(x)


	def _depthwise_conv_block(inputs, pointwise_conv_filters, alpha=1, depth_multiplier=1, strides=(1, 1), block_id=1):

	    channel_axis = 1 if K.image_data_format() == 'channels_first' else -1
	    pointwise_conv_filters = int(pointwise_conv_filters * alpha)

	    if strides == (1, 1):
	        x = inputs
	    else:
	        x = ZeroPadding2D(((0, 1), (0, 1)), name='conv_pad_%d' % block_id)(inputs)

	    x = DepthwiseConv2D((3, 3),padding='same' if strides == (1, 1) else 'valid', 
	    						   depth_multiplier=depth_multiplier,
	                               strides=strides,
	                               use_bias=False,
	                               name='conv_dw_%d' % block_id)(x)

	    x = BatchNormalization( axis=channel_axis, name='conv_dw_%d_bn' % block_id)(x)
	    x = ReLU(6., name='conv_dw_%d_relu' % block_id)(x)

	    x = Conv2D(pointwise_conv_filters, (1, 1),padding='same', use_bias=False, strides=(1, 1), name='conv_pw_%d' % block_id)(x)
	    
	    x = BatchNormalization(axis=channel_axis,name='conv_pw_%d_bn' % block_id)(x)

	    return ReLU(6., name='conv_pw_%d_relu' % block_id)(x)


	x = _conv_block(img_input, 32, alpha, strides=(2, 2))
	x = _depthwise_conv_block(x, 64, alpha, depth_multiplier, block_id=1)

	x = _depthwise_conv_block(x, 128, alpha, depth_multiplier, strides=(2, 2), block_id=2)
	x = _depthwise_conv_block(x, 128, alpha, depth_multiplier, block_id=3)

	x = _depthwise_conv_block(x, 512, alpha, depth_multiplier, strides=(2, 2), block_id=4)

	x = _depthwise_conv_block(x, 512, alpha, depth_multiplier, block_id=5)
	x = _depthwise_conv_block(x, 512, alpha, depth_multiplier, block_id=6)

	x = _depthwise_conv_block(x, 1024, alpha, depth_multiplier, strides=(1, 1), block_id=7)
	x = _depthwise_conv_block(x, 1024, alpha, depth_multiplier, block_id=8)

	x = AveragePooling2D((7,7),strides =(1,1), name='avg_pool')(x)
    # x = Flatten(name='flatten')(x)
   	# x = Dense( 1024, activation='relu', name='fully_connected')(x)
    # x = Dense( 1000, activation='softmax', name='predictions')(x)
    
	o = ( ZeroPadding2D( (1,1) , data_format='channels_last' ))(x)
	o = ( SeparableConv2D(256, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)

	o = ( UpSampling2D( (2,2), data_format='channels_last'))(o)
	o = ( ZeroPadding2D( (1,1), data_format='channels_last'))(o)
	o = ( SeparableConv2D( 128, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)

	o =  SeparableConv2D( n_classes , (3, 3) , padding='same', data_format='channels_last' )( o )
	o_shape = Model(img_input , o ).output_shape
	outputHeight = o_shape[1]
	outputWidth = o_shape[2]

	# o = (Reshape((  -1, outputHeight*outputWidth   )))(o)
	# o = (Permute((2, 1)))(o)

	o = (Reshape((  outputHeight*outputWidth, -1   )))(o)
	o = (Activation('softmax'))(o)
	model = Model( img_input , o )
	model.outputWidth = outputWidth
	model.outputHeight = outputHeight

	teststr = str(model.outputWidth) + " is the outputwidth"
	f= open("testparams.txt","w+")
	f.write(teststr)
	f.close()

	return model

if __name__ == '__main__':
	m = MobileNet(2)
	
