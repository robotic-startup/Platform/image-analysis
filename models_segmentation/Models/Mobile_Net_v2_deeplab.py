from keras.models import *
from keras.layers import *
from keras import backend as K
from keras.initializers import glorot_normal
import tensorflow as tf
import os

file_path = os.path.dirname( os.path.abspath(__file__) )

''' alpha : The width multiplier for the model.
            The number of input and output channels at each layer are multiplied by this value.
            Default is 1.0, provides a quadratic reduction in complexity.

    depth_multiplier : The resolution multiplier. 
            The resolution of the input image and the subsequent internal represantations are multiplied by this value.
            Default is 1.0, provides a quadratic reduction in complexity.

    See the MobileNet paper for more info: https://arxiv.org/pdf/1704.04861.pdf
'''


def MobileNet(n_classes, input_height=800, input_width=1400,**kwargs):

    img_input = Input(shape=(input_height,input_width,3))
    channel_axis = 1 if K.image_data_format() == 'channels_first' else -1

    alpha = kwargs["alpha"]
    depth_multiplier = kwargs["depth_multiplier"]
    

    def _make_divisible(v, divisor, min_value=None):
        if min_value is None:
            min_value = divisor
        new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
        # Make sure that round down does not go down by more than 10%.
        if new_v < 0.9 * v:
            new_v += divisor
        return new_v

    def _conv_block(inputs, filters, alpha=1, kernel=(3, 3), strides=(1, 1)):

        filters = int(filters * alpha)
        x = ZeroPadding2D(padding=((0, 1), (0, 1)), name='conv1_pad')(inputs)
        x = Conv2D(filters, kernel, padding='valid', use_bias=False, strides=strides, name='conv1')(x)
        x = BatchNormalization(axis=channel_axis, name='conv1_bn')(x)
        return ReLU(6., name='conv1_relu')(x)

    def _inverted_res_block(inputs, expansion, stride, alpha, filters, block_id):

        in_channels = K.int_shape(inputs)[channel_axis]
        pointwise_conv_filters = int(filters * alpha)
        pointwise_filters = _make_divisible(pointwise_conv_filters, 8)
        x = inputs
        prefix = 'block_{}_'.format(block_id)

        if block_id:
            # Expand
            x = Conv2D(expansion * in_channels,kernel_size=1,padding='same',use_bias=False,activation=None,
                                               name=prefix + 'expand')(x)

            x = BatchNormalization(axis=channel_axis,epsilon=1e-3,momentum=0.999,name=prefix + 'expand_BN')(x)
            x = ReLU(6., name=prefix + 'expand_relu')(x)

        else:
            prefix = 'expanded_conv_'

        # Depthwise
        if stride == 2:
            img_dim = 2 if K.image_data_format() == 'channels_first' else 1
            input_size = K.int_shape(x)[img_dim:(img_dim + 2)]

            if input_size[0] is None:
                adjust = (1,1)
            else:
                adjust = (1 - input_size[0] % 2, 1 - input_size[1] % 2)

            correct_padding = ((1 - adjust[0], 1), (1 - adjust[1], 1))

            x = ZeroPadding2D(padding=correct_padding, name=prefix + 'pad')(x)

        x = DepthwiseConv2D(kernel_size=3,strides=stride,activation=None, use_bias=False,
                                   padding='same' if stride == 1 else 'valid',name=prefix + 'depthwise')(x)

        x = BatchNormalization(axis=channel_axis,epsilon=1e-3,momentum=0.999,name=prefix + 'depthwise_BN')(x)

        x = ReLU(6., name=prefix + 'depthwise_relu')(x)

        # Project
        x = Conv2D(pointwise_filters,kernel_size=1,padding='same',use_bias=False,activation=None,name=prefix + 'project')(x)
        x = BatchNormalization(axis=channel_axis,epsilon=1e-3,momentum=0.999,name=prefix + 'project_BN')(x)

        if in_channels == pointwise_filters and stride == 1:
            return Add(name=prefix + 'add')([inputs, x])
        return x



    def SepConv_BN(x, filters, prefix, stride=1, kernel_size=3, rate=1, depth_activation=False, epsilon=1e-3):
    
        if stride == 1:
            depth_padding = 'same'
        else:
            kernel_size_effective = kernel_size + (kernel_size - 1) * (rate - 1)
            pad_total = kernel_size_effective - 1
            pad_beg = pad_total // 2
            pad_end = pad_total - pad_beg
            x = ZeroPadding2D((pad_beg, pad_end))(x)
            depth_padding = 'valid'

        if not depth_activation:
            x = Activation('relu')(x)
        x = DepthwiseConv2D((kernel_size, kernel_size), strides=(stride, stride), dilation_rate=(rate, rate),
                            padding=depth_padding, use_bias=False, name=prefix + '_depthwise')(x)
        x = BatchNormalization(name=prefix + '_depthwise_BN', epsilon=epsilon)(x)
        if depth_activation:
            x = Activation('relu')(x)
        x = Conv2D(filters, (1, 1), padding='same',
                   use_bias=False, name=prefix + '_pointwise')(x)
        x = BatchNormalization(name=prefix + '_pointwise_BN', epsilon=epsilon)(x)
        if depth_activation:
            x = Activation('relu')(x)

        return x

    x = _conv_block(img_input, 32, alpha, strides=(2, 2))

    x = _inverted_res_block(x, filters=16, alpha=alpha, stride=1, expansion=1, block_id=0)

    x = _inverted_res_block(x, filters=24, alpha=alpha, stride=2, expansion=6, block_id=1)
    x = _inverted_res_block(x, filters=24, alpha=alpha, stride=1, expansion=6, block_id=2)
    
    x = (ZeroPadding2D( (2,0) , data_format='channels_last'))(x)
    
    x = _inverted_res_block(x, filters=32, alpha=alpha, stride=2, expansion=6, block_id=3)
    x = _inverted_res_block(x, filters=32, alpha=alpha, stride=1, expansion=6, block_id=4)
    #x = _inverted_res_block(x, filters=32, alpha=alpha, stride=1, expansion=6, block_id=5)

    x = (ZeroPadding2D( (2,0) , data_format='channels_last'))(x)

    x = _inverted_res_block(x, filters=64, alpha=alpha, stride=2, expansion=6, block_id=6)
    x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, expansion=6, block_id=7)
    #x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, expansion=6, block_id=8)
    #x = _inverted_res_block(x, filters=64, alpha=alpha, stride=1, expansion=6, block_id=9)

    x = (ZeroPadding2D( (2,0) , data_format='channels_last'))(x)

    x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, expansion=6, block_id=10)
    #x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, expansion=6, block_id=11)
    #x = _inverted_res_block(x, filters=96, alpha=alpha, stride=1, expansion=6, block_id=12)

    x = (ZeroPadding2D( (2,0) , data_format='channels_last'))(x)

    x = _inverted_res_block(x, filters=128, alpha=alpha, stride=2, expansion=6, block_id=13)
    x = _inverted_res_block(x, filters=128, alpha=alpha, stride=1, expansion=6, block_id=14)
    #x = _inverted_res_block(x, filters=160, alpha=alpha, stride=1, expansion=6, block_id=15)
    
    #x = (ZeroPadding2D( (2,0) , data_format='channels_last'))(x)

    x = _inverted_res_block(x, filters=192, alpha=alpha, stride=1, expansion=6, block_id=16)

    # last_block_filters = 1280
    # x = Conv2D(last_block_filters,kernel_size=1,use_bias=False,name='Conv_1')(x)
    # x = BatchNormalization(axis=channel_axis,epsilon=1e-3,momentum=0.999,name='Conv_1_bn')(x)
    # x = ReLU(6., name='out_relu')(x)

    # x = AveragePooling2D((7,7),strides =(1,1), name='avg_pool')(x)
    # x = Flatten(name='flatten')(x)
       # x = Dense( 1024, activation='relu', name='fully_connected')(x)
    # x = Dense( 1000, activation='softmax', name='predictions')(x)
    
    # DECODER 
    b4 = GlobalAveragePooling2D()(x)
    # from (b_size, channels)->(b_size, 1, 1, channels)
    b4 = Lambda(lambda x: K.expand_dims(x, 1))(b4)
    b4 = Lambda(lambda x: K.expand_dims(x, 1))(b4)

    b4 = Conv2D(128, (1, 1), padding='same',use_bias=False, name='image_pooling')(b4)
    b4 = BatchNormalization(name='image_pooling_BN', epsilon=1e-5)(b4)
    b4 = Activation('relu')(b4)
    # upsample. have to use compat because of the option align_corners
    size_before = K.int_shape(x)

    # workaround a bug, its not supposed to make any sense
    def resizeLayer(prev, size):
        from tf.compat.v1.image import resize_images
        l = Lambda(lambda x: resize_images(x, size, method='bilinear', align_corners=True))(prev)
        return l

    b4 = resizeLayer(b4, size_before[1:3])
    # simple 1x1
    b0 = Conv2D(128, (1, 1), padding='same', use_bias=False, name='aspp0')(x)
    b0 = BatchNormalization(name='aspp0_BN', epsilon=1e-5)(b0)
    b0 = Activation('relu', name='aspp0_activation')(b0)

     # rate = 6   
    b1 = SepConv_BN(x, 128, 'aspp1',rate=6, depth_activation=True, epsilon=1e-5)
    # rate = 12 (24)
    b2 = SepConv_BN(x, 128, 'aspp2',rate=12, depth_activation=True, epsilon=1e-5)
    # rate = 18 (36)
    b3 = SepConv_BN(x, 128, 'aspp3',rate=18, depth_activation=True, epsilon=1e-5)

    x = Concatenate()([b4,b0,b1,b2,b3])

    x = Conv2D(192, (1, 1), padding='same',use_bias=False, name='concat_projection')(x)
    x = BatchNormalization(name='concat_projection_BN', epsilon=1e-5)(x)
    x = Activation('relu')(x)
    x = Dropout(0.2)(x)

    o_shape = (16,48)
    x = Conv2D(n_classes, (1, 1), padding='same', name="last_layer_name")(x)
    x = resizeLayer(x, o_shape)
    x = (Reshape((o_shape[0]*o_shape[1], -1)))(x)
    x = (Activation('softmax'))(x)

    model = Model(img_input, x)

    model.outputWidth = o_shape[0]
    model.outputHeight = o_shape[1]

    return model

if __name__ == '__main__':
    m = MobileNet(2)
    
