
from keras.models import *
from keras.layers import *
from keras.initializers import glorot_normal


import os
file_path = os.path.dirname( os.path.abspath(__file__) )
#VGG_Weights_path = file_path+"/../data/vgg16_weights_th_dim_ordering_th_kernels.h5"


def OwnModel( n_classes ,  input_height=800, input_width=1400):

	img_input = Input(shape=(input_height,input_width,3))

	#block1
	x = SeparableConv2D(32, (3, 3), activation='relu', padding='same', name='block1_conv1', data_format='channels_last' , kernel_initializer=glorot_normal())(img_input)
	x = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool', data_format='channels_last' )(x)
	x = ( BatchNormalization(axis=-1))(x)	

	#block2
	x = SeparableConv2D(64, (3, 3), strides= (2,2,), activation='relu', padding='same', name='block2_conv1', data_format='channels_last' , kernel_initializer=glorot_normal())(x)
	x = SeparableConv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2', data_format='channels_last' , kernel_initializer=glorot_normal())(x)
	x = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool', data_format='channels_last' )(x)
	x = ( BatchNormalization(axis=-1))(x)	
	
	#block3
	x = SeparableConv2D(128, (3, 3), activation='relu', padding='same', name='block3_conv1', data_format='channels_last' , kernel_initializer=glorot_normal())(x)
	x = SeparableConv2D(128, (3, 3), activation='relu', padding='same', name='block3_conv2', data_format='channels_last' )(x)
	x = SeparableConv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3', data_format='channels_last' )(x)
	x = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool', data_format='channels_last' )(x)
	x = ( BatchNormalization(axis=-1))(x)	

	#block4
	x = SeparableConv2D(256, (3, 3), activation='relu', padding='same', name='block4_conv1', data_format='channels_last' , kernel_initializer=glorot_normal())(x)
	x = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool', data_format='channels_last' )(x)
	x = ( BatchNormalization(axis=-1))(x)	

    #block5
	x = SeparableConv2D(256, (3, 3), activation='relu', padding='same', name='block5_conv1', data_format='channels_last' , kernel_initializer=glorot_normal())(x)
	x = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool', data_format='channels_last' )(x)
	x = ( BatchNormalization(axis=-1))(x)	

	o = x

	o = ( ZeroPadding2D( (1,1) , data_format='channels_last' ))(o)
	o = ( SeparableConv2D(256, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)

	o = ( UpSampling2D( (2,2), data_format='channels_last'))(o)
	o = ( ZeroPadding2D( (1,1), data_format='channels_last'))(o)
	o = ( SeparableConv2D( 128, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)

	o = ( UpSampling2D( (2,2), data_format='channels_last'))(o)
	o = ( ZeroPadding2D( (1,1), data_format='channels_last'))(o)
	o = ( SeparableConv2D( 32, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)

	o = ( UpSampling2D( (2,2), data_format='channels_last'))(o)
	o = ( ZeroPadding2D( (1,1), data_format='channels_last'))(o)
	o = ( SeparableConv2D( 8, (3, 3), padding='valid', data_format='channels_last'))(o)
	o = ( BatchNormalization(axis=-1))(o)



	o =  SeparableConv2D( n_classes , (3, 3) , padding='same', data_format='channels_last' )( o )
	
	o_shape = Model(img_input , o ).output_shape
	outputHeight = o_shape[1]
	outputWidth = o_shape[2]

	o = (Reshape((  outputHeight*outputWidth, -1   )))(o)

	o = (Activation('softmax'))(o)
	model = Model( img_input , o )
	model.outputWidth = outputWidth
	model.outputHeight = outputHeight

	return model


if __name__ == '__main__':
	m = OwnModel( 101 )
	
