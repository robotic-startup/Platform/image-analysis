import numpy as np 
import cv2
import os
import LoadBatches
import random
import augmentation_functions as aug 
import glob
import pathlib
import tensorflow as tf
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 


def getImageArr(image_path, width, height, imgNorm):
    # img = LoadBatches.getImageArr(imgname, width, height, imgNorm)

    img = tf.io.read_file(image_path)

    # convert the compressed string to a 3D uint8 tensor
    img = tf.image.decode_jpeg(img, channels=3)
    # Use `convert_image_dtype` to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img, tf.float32)
    # resize the image to the desired size.
    img=  tf.image.resize(img, [height, width])

    if imgNorm =="sub_mean":
        channel_mean = tf.constant([71.0, 100.0, 97.0])

        img = tf.subtract(img, channel_mean)
    #     img[:,:,0] -= 71
    #     img[:,:,1] -= 100
    #     img[:,:,2] -= 97
    # elif imgNorm == "sub_mean_image":
    #     img[:,:,0] -= np.mean(img[:,:,0])
    #     img[:,:,1] -= np.mean(img[:,:,1])
    #     img[:,:,2] -= np.mean(img[:,:,2])
    # img = tf.expand_dims(img, 0)
    return img

def getSegmentationArr( path , nClasses ,  output_width , 
            output_height, annotation_filter = "oldschool"):

    if(annotation_filter=="oldschool"):
        seg_labels = np.zeros((  output_height , output_width  , nClasses ))
        

        # img_np = cv2.imread("/home/ozastrow/image-analysis/dataset_current/tmpmoved/annotations_val/picture_14.png")
        img_np = np.ones((output_height, output_width, 3))*0.5
        try:
            img_np = cv2.imread(path)
        except e:
            print("couldn't read")
        img_np = cv2.resize(img_np, (output_width, output_height))[:,:,0]
        # img_np[:10, :10] = 0
        # img_np[:5, :5] = 1
        for c in range(nClasses):
            seg_labels[: , : , c ] = (img_np == c ).astype(int)

        seg_labels = np.reshape(seg_labels.astype(np.float32), (output_height*output_width, 2))
        # print(np.unique(seg_labels))
        # if(len(np.unique(seg_labels)))==1:
        #     print(path.numpy())
        seg_labels = tf.convert_to_tensor(seg_labels)
        # print(path)
        # if(not os.path.exists("/home/ozastrow/image-analysis/dataset_current/tmpmoved/annotations_val/picture_: - 14.png")):
        #     pass
            # print(str(path))
        # img = tf.io.read_file(str(path.numpy(), "utf-8") )
        # img = tf.io.read_file(path)
        # img = tf.image.decode_png(img, channels=3)
        # img = tf.convert_to_tensor(img, dtype=tf.float32)
        # img_class1 = tf.image.rgb_to_grayscale(img)
        
        
        # img=  tf.image.resize(img, [output_height, output_width], method=tf.image.ResizeMethod.BILINEAR)
        # img_class1 = img[:,:,0]
        # img_class1 = tf.image.convert_image_dtype(img_class1, tf.uint8)

        
        # img_class2 = tf.identity(img_class1)
        # img_class2 = tf.subtract(img_class2, 1)
        # img_class2 = tf.multiply(img_class2, -1)
        # seg_labels = tf.stack([img_class1, img_class2], axis=-1)
        # seg_labels = tf.reshape(seg_labels, ( output_width*output_height , nClasses ))
        # seg_labels = tf.image.convert_image_dtype(seg_labels, tf.float32)

    return seg_labels


def prepare_for_training(ds, is_train, cache=True, shuffle_buffer_size=1000, BATCH_SIZE=32):
  # This is a small dataset, only load it once, and keep it in memory.
  # use `.cache(filename)` to cache preprocessing work for datasets that don't
  # fit in memory.
#   if cache:
#     if isinstance(cache, str):
#       ds = ds.cache(cache)
#     else:
#       ds = ds.cache()

  ds = ds.shuffle(buffer_size=shuffle_buffer_size)

  # Repeat forever
  ds = ds.repeat()

  ds = ds.batch(BATCH_SIZE)

  # `prefetch` lets the dataset fetch batches in the background while the model
  # is training.
  ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

  return ds

def show_batch(image_batch, label_batch):
    plt.figure(figsize=(18,12))
    for n in range(0, 16, 2):
        ax = plt.subplot(8, 2,n+1)
        plt.imshow(image_batch[n][:,:,0])
        ax = plt.subplot(8, 2,n+2)
        tmp_label = label_batch[n].reshape((16, 48, 2))[:,:,0]
        plt.imshow(tmp_label)
        plt.axis('off')
    plt.savefig("temp_visualization.png")

def timeit(ds, steps, BATCH_SIZE):
        start = time.time()
        it = iter(ds)
        for i in range(steps):
            batch = next(it)
            if i%10 == 0:
                print('.',end='')
        print()
        end = time.time()

        duration = end-start
        print("{} batches: {} s".format(steps, duration))
        print("{:0.5f} Images/s".format(BATCH_SIZE*steps/duration))
        image_batch, label_batch = next(iter(ds))

def load_dataset(train_images_path, val_images_path, augmentations, 
                    intensities, imgNorm, input_height, input_width, nClasses, 
                    annotation_filter, output_width, output_height, 
                    is_student, teacher_segs_path , BATCH_SIZE):
    
    ##check wheter dataset size makes sense
    # import os
    # files = os.listdir(val_images_path)
    # pngs = [f for f in files if (".png" in f)]
    # jpgs = [f for f in files if (".jpg" in f)]
    # pngs.sort()
    # jpgs.sort()
    # print("len if pngs is " + str(len(pngs)) + " len of jpgs is " + str(len(jpgs)))

    def process_path(file_path):
        img = getImageArr(image_path=file_path, width=input_width, height=input_height, imgNorm=imgNorm)
        file_path_gt = tf.identity(file_path)
        file_path_gt = tf.strings.regex_replace((file_path_gt), ".jpg", ".png").numpy().decode("utf-8")
        gt = getSegmentationArr(path=file_path_gt, nClasses=nClasses, output_width = output_width, 
                        output_height=output_height, annotation_filter=annotation_filter)
        
        return img, gt


    assert train_images_path[-1] == '/'
    assert val_images_path[-1] == '/'

    # images = glob.glob( images_path + "*.jpg") + glob.glob( images_path + "*.png"  ) +  glob.glob( images_path + "*.jpeg"  )
    # images.sort()

    # gt_segmentations = glob.glob( gt_segs_path + "*.jpg") +glob.glob( gt_segs_path + "*.png"  ) +  glob.glob( gt_segs_path + "*.jpeg"  )
    # gt_segmentations.sort()

    # #check validity of dataset
    # assert len( images ) == len(gt_segmentations) and len(images)> 0
    # for im , seg in zip(images,gt_segmentations):
    #     assert(  im.split('/')[-1].split(".")[0] ==  seg.split('/')[-1].split(".")[0] )
    
    
    data_train_dir = pathlib.Path(train_images_path)
    list_train_ds = tf.data.Dataset.list_files(str(data_train_dir/'*.jpg'))
    # labeled_train_ds = list_train_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    labeled_train_ds = list_train_ds.map(lambda x: tf.py_function(process_path, [x], (tf.float32, tf.float32)), num_parallel_calls=tf.data.experimental.AUTOTUNE)

    labeled_train_ds = prepare_for_training(labeled_train_ds, is_train=True, cache="traindscache")

    data_val_dir = pathlib.Path(val_images_path)
    list_val_ds = tf.data.Dataset.list_files(str(data_val_dir/'*.jpg'))
    # labeled_val_ds = list_val_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    labeled_val_ds = list_val_ds.map(lambda x: tf.py_function(process_path, [x], (tf.float32, tf.float32)), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    labeled_val_ds = prepare_for_training(labeled_val_ds, is_train=False, cache="valdscache")

    
    return labeled_train_ds, labeled_val_ds

if __name__=="__main__":
    #the following codeblock is only for testing
    labeled_train_ds, labeled_val_ds = load_dataset(train_images_path="../dataset_current/dataset_own_v9.1_reordered/annotations_train/",
                        val_images_path="../dataset_current/dataset_own_v9.1_reordered/annotations_val/",
                        augmentations=[aug.random_zoom], intensities=[0.2],
                        imgNorm="sub_mean", input_width=384, input_height=130, nClasses=2, 
                        annotation_filter="oldschool", output_width=48, output_height=16,
                        is_student=False, teacher_segs_path="", BATCH_SIZE=128
                        )

    default_timeit_steps = 200
    # timeit(labeled_train_ds, steps=default_timeit_steps, BATCH_SIZE=BATCH_SIZE)    
    image_batch, label_batch = next(iter(labeled_train_ds))
    show_batch(image_batch.numpy(), label_batch.numpy())


    for image, label in labeled_train_ds.take(1):
        print("Image shape: ", image.numpy()[0].shape)
        print("Image shape: ", label.numpy()[0].shape)

