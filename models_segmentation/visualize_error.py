import argparse
import LoadBatches
from tensorflow.keras.models import load_model
import glob
from tensorflow.keras import backend as K
from tensorflow.keras import losses
from tensorflow.keras import models
import Models
import cv2
import numpy as np
import random
import time
import tensorflow as tf

import os
from datetime import datetime
from tensorflow.keras import losses
import metrics_and_losses
import augmentation_functions as aug

parser = argparse.ArgumentParser()

parser.add_argument("--images", type = str , default = "../dataset_current/dataset_own_v8.3/images_val/")
parser.add_argument("--model_file", type = str, default = "selected_trained_models/vgg_half.hdf5",
                    help="the file can be hdf5  or h5, tflite not supported yet")
parser.add_argument("--annotation_filter", type=str, default="oldschool", help="choose between oldschool, dilated, smoothed")
parser.add_argument("--annotations", type = str, default="../dataset_current/dataset_own_v8.3/annotations_val/")    
parser.add_argument("--annotations_teacher", type = str, default="../dataset_current/distilled5/images_val/")    
parser.add_argument("--save_output", action="store_true", default=False,
                    help="results will be stored in error_visualization dir")
parser.add_argument("--visualize", action="store_true", default=False, 
                    help="results will be stored in error_visualization dir")
parser.add_argument('--is_tflitemodel', help='set if you want to use a tflite model for inference', action='store_true', default=False)
parser.add_argument('--sortbydate', help='set if you want to sort the saved output by the image file name', action='store_true', default=False)
parser.add_argument('--is_student',action="store_true",default=False)
parser.add_argument('--temp', help="temperature of distilled network output", default=5)

print("INFO: use --visualize flag, to show output per image directly instead of saving it")


args = parser.parse_args()

if(args.visualize or args.save_output):
    pass
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('tkAGG')

n_classes = 2
images_path = args.images
model_file = args.model_file
is_tflitemodel = args.is_tflitemodel
annotations_path = args.annotations


def visualize(X, pr, imgName, img_orig, output_file_path, annotations_path, loss_sum, iou_sum, hard_iou_sum, images_path):
    plt.rcParams.update({'font.size': 8})
    
    annotationName = annotations_path  + imgName[:-4] + ".png"

    print(annotationName, annotations_path)
    y_true = LoadBatches.getSegmentationArr(annotationName, 2, output_width, output_height, annotation_filter=args.annotation_filter)
    y_pred = pr
    y_pred_2d = np.reshape(pr[:,:2], (output_height, output_width, 2))
    y_true_2d = np.reshape(y_true[:,:2], (output_height, output_width, 2))
    

    assert y_true is not None
    teachers_opinion_img = np.zeros((output_height, output_width))
    if (args.is_student):
        teacherAnnotations = args.annotations_teacher + imgName[:-4] + ".png"
        assert(args.annotations_teacher[-1] =="/")
        teachers_opinion= LoadBatches.getSegmentationArr(teacherAnnotations,2,output_width,output_height, annotation_filter="teacher")
        teachers_opinion_img = np.reshape(teachers_opinion, (output_height, output_width, 2))[:,:,0]
        
        y_true = np.concatenate([y_true, teachers_opinion], axis=1).astype(np.float32)
        y_true_2d = np.reshape(y_true, (output_height, output_width, n_classes*2))
        y_pred_2d = np.reshape(y_pred, (output_height, output_width, n_classes*2))

        argmaxpred = y_pred_2d[:,:,2:].argmax(axis=-1)
    else: 
        argmaxpred = y_pred_2d.argmax(axis=2)


    fig, axs = plt.subplots(nrows=3, ncols=3)
    plt.rcParams.update({'font.size': 8})

    #ground trouth annotation
    ax = axs[0, 0]
    ax.set_title("Ground Truth")
    ax.imshow(y_true_2d[:,:,0])
    
    #orig image
    ax = axs[0,1]
    ax.imshow(img_orig)
    ax.set_title("Image")

    #teacher Ground Trutth
    ax = axs[0, 2]
    if(not args.is_student):
        ax.set_title("cat pictures")

    else: 
        ax.set_title("Teacher Ground Truth")
        ax.imshow(teachers_opinion_img)


    ax = axs[1,0]
    if(not args.is_student):
        ax.set_title("cat pictures")
        pass
    else: 
        ax.set_title("loss soft")
        temp = args.temp
        
        loss = metrics_and_losses.distilled_loss_teacher(temp)
        loss_soft = K.eval(loss(y_true_2d, y_pred_2d))
        loss_soft = np.reshape(loss_soft, (output_height, output_width, 2))[:,:,0]
        ax.imshow(loss_soft)

    #loss overall
    ax = axs[2,2]
    loss_gt = None
    if(args.is_student):
        ax.set_title("loss hard")
        # ax.set_title("y_pred :,:,3")
        loss_hard = K.eval(metrics_and_losses.distilled_loss_gt()(y_true_2d, y_pred_2d))[:,:,0]
        loss_gt = loss_hard
        ax.imshow(loss_hard)
        #ax.imshow(y_pred_2d[:,:,3])

    else:
        ax.set_title("binary loss")
        loss_gt = K.eval(metrics_and_losses.binary_log_loss()(y_true, y_pred))
        
        loss_gt = np.reshape(loss_gt, (output_height, output_width, 2))
        ax.imshow(loss_gt[:,:,1])

    colors = []

    for i in range(n_classes):
        br = int(255/(n_classes-1))*i
        colors.append((br, br, br))

    seg_img_rgb = np.zeros( ( output_height, output_width , 3  ) )
    for c in range(n_classes):
        seg_img_rgb[:,:,0] += ((argmaxpred[:,: ] == c )*( colors[c][0] )).astype('uint8')
        seg_img_rgb[:,:,1] += ((argmaxpred[:,: ] == c )*( colors[c][1] )).astype('uint8')
        seg_img_rgb[:,:,2] += ((argmaxpred[:,: ] == c )*( colors[c][2] )).astype('uint8')
    seg_img_rgb = cv2.resize(seg_img_rgb  , (input_width , input_height ), interpolation=cv2.INTER_NEAREST)
    
    resized_img_orig = cv2.resize(img_orig, ((input_width , input_height  )))
    overlay = (0.5 * resized_img_orig + 0.5* seg_img_rgb).astype(np.uint8)

    
    #overlay
    ax = axs[1, 1]
    ax.set_title("prediction-groundtruth-overlay")
    ax.imshow(overlay)

    ax = axs[1,2]
    ax.set_title("Soft Prediction")
    ax.imshow(y_pred_2d[:,:,0])

    ax = axs[2,1]

    if(args.is_student):
        #loss both
        ax.set_title("distilled loss both")
        loss_combined = K.eval(metrics_and_losses.distilled_loss_both(args.temp)(y_true_2d, y_pred_2d))[:,:,0]
        ax.imshow(loss_combined)
    else:
        #fp error function
        fp_error = metrics_and_losses.calc_fp_error(y_true, y_pred)
        fp_error = fp_error.reshape((output_height,output_width, 2))
        ax.set_title("FP Error")
        ax.imshow(fp_error[:,:,0])

    #fn error function
    fn_error = metrics_and_losses.calc_fn_error(y_true, y_pred)
    # fn_error = fn_error.reshape((output_height, output_width, 2))
    ax = axs[2,0]
    ax.set_title("FN Error")
    ax.set_title("argmaxpred")
    # ax.imshow(fn_error[:,:,0])
    #a2d = np.reshape(argmaxpred_flat, (output_height, output_width, 2))
    ax.imshow(argmaxpred)

    mng = plt.get_current_fig_manager()
    mng.resize(width=1920,height=1080)

    if (args.is_student):
        log_err = np.mean(loss_combined)
        hard_iou = np.mean(loss_gt)
        
        iou_soft = np.mean(loss_soft)
        title = images_path + imgName + "\n loss: " + str(log_err)[:5]+"\n hard iou: "+ str(hard_iou)[:5] + "  soft_iou: " + str(iou_soft)[:5]
        iou_sum.append(iou_soft)
        hard_iou_sum.append(hard_iou)

    else:
        log_err = np.mean(loss_gt)
        iou = np.mean(K.eval(metrics_and_losses.iou_score(y_true_2d, y_pred_2d)))
        title = images_path + imgName + "\n loss: " + str(log_err)[:5] +  "\n iou: " + str(iou)[:5]
        iou_sum.append(iou)

    fig.suptitle(title)
    loss_sum.append(log_err)

    if(args.save_output):
        if(args.sortbydate):
            saved_output_name = output_file_path + '/' + imgName
        else:
            saved_output_name = output_file_path + '/_log_error_' + str(log_err*10000) + '_' + imgName
        ticsavefig = time.time()
        plt.savefig(saved_output_name, orientation="landscape", dpi=300)
        tocsavefig = time.time()
        print("savefig takes " + str(tocsavefig-ticsavefig)) 
    else:
        plt.show()
    
    plt.clf()  #clears the figure

#load model
if(is_tflitemodel):
	
    # Load TFLite model and allocate tensors.
    interpreter = tf.lite.Interpreter(model_path=model_file)

    # interpreter.setNumThreads(4)
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    input_shape = input_details[0]['shape']
    output_shape = output_details[0]['shape']

    input_height = input_shape[1]
    input_width = input_shape[2]

    output_width = 48
    output_height = 16


    
    assert(output_width * output_height == output_shape[1])
else:

	#if model is kerasmodel not tflitemodel
    if(args.is_student):
        temp = args.temp
        m = load_model(model_file,custom_objects={
                                'iou_score_student_soft':metrics_and_losses.iou_score_student_soft,
                                'iou_score_student_hard':metrics_and_losses.iou_score_student_hard,
                                'loss_both':metrics_and_losses.distilled_loss_both(temp=temp),
                                'loss_teacher':metrics_and_losses.distilled_loss_teacher(temp=temp),
                                'loss_gt':metrics_and_losses.distilled_loss_gt(),
                                })
        
        last_layer_shape = m.layers[-6].output_shape
    else:
        m = load_model(model_file, custom_objects={'iou_score':metrics_and_losses.iou_score, 'iou_score_student_hard':metrics_and_losses.iou_score_student_hard})
        last_layer_shape = m.layers[-3].output_shape  

    first_layer_shape = m.layers[0].output_shape[0]
    print(first_layer_shape)
    input_width = first_layer_shape[2]
    input_height = first_layer_shape[1]
    output_width = last_layer_shape[2]
    output_height = last_layer_shape[1]



error_vis_path = 'error_visualization/' + 'visualization_' + str(datetime.now())[:-7]+ '/'


def iterate_through_folder(images_path):
    annotations_path = args.annotations
    segmentation_timer = []
    loss_sum = []
    iou_sum = []
    hard_iou_sum = []

    sub_dir_path = os.path.relpath(os.path.normpath(images_path), os.path.normpath(args.images)) + "/"
    output_file_path = os.path.normpath(error_vis_path + sub_dir_path)
    
    if(args.save_output):
        if not os.path.exists(output_file_path):
            os.makedirs(output_file_path)
    counter = 0

    files = os.listdir(images_path)
    pngs = [f for f in files if (".png" in f)]
    jpgs = [f for f in files if (".jpg" in f)]
    pngs.sort()
    jpgs.sort()
    if(len(pngs)>0 and len(jpgs)> 0):
        #this means the annotations are in the same folder as the images
        annotations_path = images_path

    
    for imgName in jpgs:
        counter +=1

        if counter == 100:
            break

        img_orig = cv2.imread(images_path + imgName)

        X = LoadBatches.getImageArr(images_path + imgName , input_width  , input_height )

        #start measuring time
        tic = time.time()

        if(is_tflitemodel):
            input_data = [X]
            interpreter.set_tensor(input_details[0]['index'], input_data)
            interpreter.invoke()

            pr = interpreter.get_tensor(output_details[0]['index'])[0]

        else:
            pr = m.predict( np.array([X]) )[0]

        #stop measuring time
        toc = time.time()
        segmentation_timer.append(toc-tic)


        inference_ms = str(int(segmentation_timer[-1]*1000))
        print(inference_ms + "ms, " + str(counter) + '/' + str(len(jpgs)) + " showing image overview of: " +  imgName)

        if(args.visualize or args.save_output):
            visualize(X, pr, imgName, img_orig, output_file_path, annotations_path, loss_sum, iou_sum, hard_iou_sum, images_path)
        


    def reject_outliers(data, m=0.5):
        filtered = data[abs(data - np.mean(data)) < m * np.std(data)]
        print("rejecting", len(data) - len(filtered), "outliers")
        return filtered

    if(not args.visualize):
        segmentation_timer = np.array(segmentation_timer)
        segmentation_timer = reject_outliers(segmentation_timer)

        avg_time = str(np.mean(segmentation_timer))
        total_time = str(np.sum(segmentation_timer))
        print('total segmentation duration for ' + str(counter) + ' images: ' + total_time + ' average: ' + avg_time)
    else:
        print("not printing inference speed because visualization is turned on")
    if(args.save_output):
        #calculate grades per datasubset and rename folder accordingly
        split_path = os.path.split(os.path.normpath(output_file_path))
        path_head = split_path[0]
        path_tail = split_path[1]
        assert(path_tail != "")
        renamed_path = path_head + "/"  + path_tail +  "_iou: " + str(np.mean(iou_sum)*100)[:4]
        print("renaming folder ", output_file_path, "to", renamed_path)
        os.rename(output_file_path, renamed_path)
        if(args.is_student):
            print(np.mean(hard_iou_sum), "is hard iou sum", np.mean(iou_sum), "soft_iou_sum")

def recursive_error_vis(current_dir):
    files=os.listdir(current_dir)
    files.sort()
    imgfiles = [f for f in files if (".png" in f or ".jpg" in f)]
    
    #there should be either only images or only folders in the dir
    assert not (len(files)!= len(imgfiles) and len(imgfiles)>0)
    print("running error vis on folder:", current_dir)
    
    if(len(imgfiles)==0 ):
        for f in files:
            recursive_error_vis(current_dir + "/" + f)

    else:
        #annotations and images in same folder
        iterate_through_folder(current_dir + "/")
            

recursive_error_vis(images_path)




