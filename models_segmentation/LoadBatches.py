import numpy as np
import cv2
import glob
import itertools
import time
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import random
import threading
import augmentation_functions as aug

def getImageArr( imgname , width , height , imgNorm="sub_mean" ):
    img = cv2.imread(imgname)
    # if imgNorm == "sub_and_divide":
    #     img = np.float32(cv2.resize(img, ( width , height ))) / 127.5 - 1
    if imgNorm == "sub_mean":
        img = cv2.resize(img, ( width , height ))
        img = img.astype(np.float32)
        img[:,:,0] -= 71
        img[:,:,1] -= 100
        img[:,:,2] -= 97

    elif imgNorm == "sub_mean_image":
        img = cv2.resize(img, ( width , height ))
        img = img.astype(np.float32)
        img[:,:,0] -= np.mean(img[:,:,0])
        img[:,:,1] -= np.mean(img[:,:,1])
        img[:,:,2] -= np.mean(img[:,:,2])
    # elif imgNorm == "divide":
    #     img = cv2.resize(img, ( width , height ))
    #     img = img.astype(np.float32)
    #     img = img/255.0
    else:
        img = cv2.resize(img, ( width , height ))
        #img = img.astype(np.float32)
    
    return img


def getSegmentationArr( path , nClasses ,  output_width , output_height, annotation_filter = "oldschool"):

    if(annotation_filter=="oldschool"):
        seg_labels = np.zeros((  output_height , output_width  , nClasses ))
        try:
            img = cv2.imread(path, 1)
            img = img[:, : , 0]
            img = cv2.resize(img, ( output_width , output_height ))

            for c in range(nClasses):
                seg_labels[: , : , c ] = (img == c ).astype(int)

        except Exception as e:
            print(e)
        seg_labels = np.reshape(seg_labels, ( output_width*output_height , nClasses )).astype(float)

    elif(annotation_filter=="smoothed"):

        seg_labels = np.zeros((  output_height , output_width  , nClasses ))
        try:
            img = cv2.imread(path).astype(float)[:,:,0]
            #of all interpolation methods available in cv2, inter_area looses the least information in our scenario
            img = cv2.resize(img, ( output_width , output_height ), interpolation=cv2.INTER_AREA)

            assert(nClasses==2)

            #for c in range(nClasses):
            #line from original segnet repo
            #seg_labels[: , : , c ] = (img == c ).astype(int)

            seg_labels[:,:,1] = img
            seg_labels[:,:,0] = (img-1)*(-1)

        except Exception as e:
            print(e)
        seg_labels = np.reshape(seg_labels, ( output_width*output_height , nClasses ))


    elif(annotation_filter=="dilated"):
        seg_labels = np.zeros((  output_height , output_width  , nClasses ))
        try:
            img = cv2.imread(path, 1)[:,:,0].astype(float)
            img = (img-1)*(-1)
            kernel = np.ones((10, 10))

            ann_dl = cv2.dilate(img, kernel)
            ann_dl = (ann_dl-1)*(-1)


            ann_dl = cv2.resize(ann_dl, ( output_width , output_height ))
            
            for c in range(nClasses):
                seg_labels[: , : , c ] = (ann_dl == c ).astype(int)

        except Exception as e:
            print(e)

        seg_labels = np.reshape(seg_labels, ( output_width*output_height , nClasses ))

    elif(annotation_filter=="teacher"):
        seg_labels = np.zeros((  output_height , output_width  , nClasses ))

        try:
            img = cv2.imread(path, 1)
            img = img[:, : , 0]
            img = cv2.resize(img, ( output_width , output_height ))
            img = img.astype(float)

            seg_labels[:,:,0] = img
            seg_labels[:,:,1] = (img-255)*(-1)

        
        except Exception as e:
            print(e)    
        
        seg_labels = np.reshape(seg_labels, ( output_width*output_height , nClasses ))
        seg_labels /= 255.0

    return seg_labels

class threadsafe_iter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()
        self.batch_size = 32 
        self.n = 320

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.it.__next__()


def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_iter(f(*a, **kw))
    return g


'''
    Yields both the ground truth segmentations and the teachers predictions in a 2-tuple.
    ACHTUNG: Y[0] --> ground truth
             Y[1] --> teachers prediction 
'''

# @threadsafe_generator ###activate for pruning
def distillationImageSegmentationGenerator( images_path , gt_segs_path ,  batch_size,  n_classes, 
                                input_height , input_width , output_height , output_width, teacher_segs_path="",
                                imgNorm ="sub_mean",  augmentations=[], intensities = [], augmentation_rate=0.3,
                                train_steps=97, annotation_filter="oldschool", is_student=False):


    assert images_path[-1] == '/'
    assert gt_segs_path[-1] == '/'

    images = glob.glob( images_path + "*.jpg") + glob.glob( images_path + "*.png"  ) +  glob.glob( images_path + "*.jpeg"  )
    images.sort()

    gt_segmentations = glob.glob( gt_segs_path + "*.jpg") +glob.glob( gt_segs_path + "*.png"  ) +  glob.glob( gt_segs_path + "*.jpeg"  )
    gt_segmentations.sort()

    assert len( images ) == len(gt_segmentations) and len(images)> 0

    for im , seg in zip(images,gt_segmentations):

        assert(  im.split('/')[-1].split(".")[0] ==  seg.split('/')[-1].split(".")[0] )

    if(is_student):

        teacher_segmentations =  glob.glob( teacher_segs_path + "*.jpg") +glob.glob( teacher_segs_path + "*.png"  ) +  glob.glob( teacher_segs_path + "*.jpeg"  )
        teacher_segmentations.sort()
        assert teacher_segs_path[-1] == '/'
        assert len( teacher_segmentations ) == len(gt_segmentations)
        
        for gt , tch in zip(gt_segmentations,teacher_segmentations):

            assert(  gt.split('/')[-1].split(".")[0] ==  tch.split('/')[-1].split(".")[0] )
    
    counter = 0
    
    if(is_student):
        zipped_list = list(zip(images, gt_segmentations, teacher_segmentations))
    
    else:
        zipped_list = list(zip(images, gt_segmentations))

    random.shuffle(zipped_list)
    zipped = itertools.cycle(zipped_list)
    steps_per_epoch = train_steps * batch_size
    #steps_per_epoch = min(32*97, steps_per_epoch)

    assert(len(augmentations)==len(intensities))
    
    while True:
        X = []
        Y = []
        
        #shuffle dataset again after every epoch
        
        if(counter>steps_per_epoch):
            print("\n shuffling images, counter is " + str(counter) + " \n")

            counter = 0
            random.shuffle(zipped_list)
            zipped = itertools.cycle(zipped_list)

        for _ in range( batch_size) :
            counter += 1

            if(is_student):

                im , seg_gt, seg_teacher = next(zipped)
                tempY_teacher = getSegmentationArr(seg_teacher , n_classes , output_width=output_width , output_height=output_height , annotation_filter = "teacher") 
                assert (seg_teacher.split("/")[-1][:-4] == seg_gt.split("/")[-1][:-4] == im.split("/")[-1][:-4])
            else:
                im, seg_gt = next(zipped)
            
            tempX = getImageArr(im , input_width , input_height, imgNorm = imgNorm )
            tempY_gt = getSegmentationArr(seg_gt , n_classes , output_width=output_width , output_height=output_height , annotation_filter = annotation_filter) 

            if len(augmentations) > 0:

                p = np.random.random(size=len(augmentations))
                s = np.random.randint(0,999999, size=len(augmentations))

                for i in range(len(augmentations)):
                    if(p[i] < augmentation_rate):
                        tempX = augmentations[i](tempX, intensity=intensities[i], seed=s[i])
                        tempY_gt = augmentations[i](np.reshape(tempY_gt,(output_height, output_width ,2)), intensity=intensities[i], seed=s[i])
                        tempY_gt = np.reshape(tempY_gt,(output_width*output_height,2))
                        if(is_student):
                            tempY_teacher = augmentations[i](np.reshape(tempY_teacher,(output_width, output_height ,2)), intensity=intensities[i], seed=s[i])
                            tempY_teacher = np.reshape(tempY_teacher,(output_width*output_height,2))
            
            tempY = tempY_gt             
            if(is_student):
                tempY = np.concatenate((tempY_gt,tempY_teacher), axis = 1)
            X.append(tempX)
            Y.append(tempY)

        Y = np.array(Y)
        X = np.array(X)

        yield  X,Y 

# import Models , LoadBatches
# G  = LoadBatches.imageSegmentationGenerator( "data/clothes_seg/prepped/images_prepped_train/" ,  "data/clothes_seg/prepped/annotations_prepped_train/" ,  1,  10 , 800 , 550 , 400 , 272   ) 
# G2  = LoadBatches.imageSegmentationGenerator( "data/clothes_seg/prepped/images_prepped_test/" ,  "data/clothes_seg/prepped/annotations_prepped_test/" ,  1,  10 , 800 , 550 , 400 , 272   ) 

# m = Models.VGGSegnet.VGGSegnet( 10  , use_vgg_weights=True ,  optimizer='adadelta' , input_image_size=( 800 , 550 )  )
# m.fit_generator( G , 512  , nb_epoch=10 )


