import matplotlib.pyplot as plt 
import os
import cv2
### this script can display predictions

pngs_path = "../dataset_current/autopredict/images_train/"
imgs_path = "../dataset_current/tempv9/obstacle/"

imgs = os.listdir(imgs_path)
imgs.sort()

pngs = os.listdir(pngs_path)
pngs.sort()

for i in range(len(pngs)):
    print(pngs_path + pngs[i])
    png = cv2.imread(pngs_path + pngs[i])
    img = cv2.imread(imgs_path + imgs[i])
    assert os.path.exists(imgs_path + imgs[i])
    print(pngs_path + pngs[i])
    plt.subplot(2, 1, 1)
    plt.imshow(png)
    plt.subplot(2, 1, 2)
    plt.imshow(img)
    plt.show()