import os
import cv2
import shutil
import numpy as np 
from tqdm import tqdm
import tensorflow as tf
images_train = "../dataset_current/dataset_own_v9.1_reordered/annotations_train/"
images_val = "../dataset_current/dataset_own_v9.1_reordered/annotations_val/"
output_tmp_train ="../dataset_current/tmpmoved/annotations_train/"
output_tmp_val ="../dataset_current/tmpmoved/annotations_val/"
output_tmp_2 = "../dataset_current/tmp2/"
import pathlib
# shutil.rmtree(output_tmp_val)
# shutil.rmtree(output_tmp_train)
# os.mkdir(output_tmp_train)
# os.mkdir(output_tmp_val)
data_dir = pathlib.Path(images_train)
# names = list(data_dir/'*.jpg')

# names = list(data_dir.glob('*.png'))
# for name in names:
#     print(str(name))
#     # tmp = tf.io.read_file(str(name))
#     # tmp_decoded = tf.image.decode_png(tmp, channels=3)
#     # tmp_resized=  tf.image.resize(tmp_decoded, [output_height, output_width], method=tf.image.ResizeMethod.BILINEAR)
#     # img_class1 = tmp_resized[:,:,0]
#     # img_numpy = img_class1.numpy()
#     # cv2.imsave("test_vis_temp/" + )

# print("done")
    


files_train = os.listdir(images_val)
pngs_train = [f for f in files_train if (".png" in f)]
jpgs_train = [f for f in files_train if (".jpg" in f)]
pngs_train.sort()
jpgs_train.sort()

found_counter = 0
# for i in tqdm(range(len(pngs_train))):
#     # img = cv2.imread(images_val + pngs_train[i])
#     # npun = np.unique(img[:,:,0])
#     # if(1 == len(npun) and npun[0] == 1):
#     #     found_counter +=1
#         # shutil.copy(images_val + pngs_train[i], output_tmp_val + pngs_train[i])
#         # shutil.copy(images_val + jpgs_train[i], output_tmp_val +jpgs_train[i])
#     os.rename(images_val + pngs_train[i], images_val + pngs_train[i].replace(":", "_"))
#     os.rename(images_val + jpgs_train[i], images_val + jpgs_train[i].replace(":", "_"))
# print(found_counter, "found counter", len(pngs_train))


import tensorflow as tf

width = 384
height = 130
input_width = width
input_height = height
imgNorm="sub_mean"
nClasses =2
output_height=16
output_width=48
annotation_filter="oldschool"
import loadDataset
def process_path(file_path):
    img = loadDataset.getImageArr(image_path=file_path, width=input_width, height=input_height, imgNorm=imgNorm)
    file_path_gt = tf.identity(file_path)

    file_path_gt = tf.strings.regex_replace((file_path_gt), ".jpg", ".png")
    gt = loadDataset.getSegmentationArr(path=file_path_gt, nClasses=nClasses, output_width = output_width, 
                    output_height=output_height, annotation_filter=annotation_filter)
    return img, gt, file_path, file_path_gt



data_train_dir = pathlib.Path(images_val)


list_train_ds = tf.data.Dataset.list_files(str(data_train_dir/'*.jpg'))

labeled_train_ds = list_train_ds.map(process_path, num_parallel_calls=tf.data.experimental.AUTOTUNE)

# for i, j in labeled_train_ds:
#     # print(i.shape, j.shape)
#     print()
# print()
# print()
# labeled_train_ds = prepare_for_training(labeled_train_ds, is_train=True, cache="traindscache")
import matplotlib.pyplot as plt 
counter = 0
for img, gt , imgpath, gtpath in labeled_train_ds:
    gtimg = gt.numpy()
   
    plt.title(gtpath)
    # plt.imshow(gtimg)

    ##check wheter getSegArray is flawed
    # print(gtpath.numpy().decode("utf-8"))
    # npimg = cv2.imread(gtpath.numpy().decode("utf-8"))[:,:,0]
    print(imgpath)
    print(gtpath)
    gt = np.reshape(gt[:, 0], (output_height, output_width))
    print()
    plt.imshow(gt)
    plt.show()
    plt.savefig(output_tmp_2 + str(counter) + ".png")
    plt.imshow(img[:,:,0])
    plt.show()
    plt.savefig(output_tmp_2 + str(counter) + "img.png")
    counter += 1
    if(counter ==150):
        break
    