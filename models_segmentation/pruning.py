from keras import backend as K
import argparse
import tensorflow as tf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from Models import Xception_deeplab as Xception
from Models.VGGSegnet_hacked_channelslast import VGGSegnet as vgg
from keras.callbacks import ModelCheckpoint, TensorBoard
import metrics_and_losses
import LoadBatches
from keras.models import Model, load_model
from tqdm import tqdm
import numpy as np
import random
import os

from kerassurgeon.identify import get_apoz, high_apoz

parser = argparse.ArgumentParser()
parser.add_argument("--save_weights_path", type = str, default="weights/pruning"  )
parser.add_argument("--train_images", type = str, default="../dataset_current/dataset_own_v8.3/images_train/"  )
parser.add_argument("--train_annotations", type = str , default="../dataset_current/dataset_own_v8.3/annotations_train/" )
parser.add_argument("--val_images", type = str , default = "../dataset_current/dataset_own_v8.3/images_val/")
parser.add_argument("--val_annotations", type = str , default = "../dataset_current/dataset_own_v8.3/annotations_val/")

parser.add_argument("--annotation_filter", type=str, default="oldschool", help="values can be smoothed, dilated or oldschool")

parser.add_argument("--epochs", type = int, default = 50 )
parser.add_argument("--train_batch_size", type = int, default = 32 )
parser.add_argument("--val_batch_size", type = int, default = 32 )
parser.add_argument("--pruning_iterations",type=int, default=5)
parser.add_argument("--model_file", type = str , default = "selected_trained_models/shufflenet_926.hdf5")
parser.add_argument("--model_type", type=str, default="xception")
parser.add_argument("--method", type=str, default='l1')
parser.add_argument("--plot_val_ious", action='store_true', default=False)

args = parser.parse_args()


def get_l1_norms(model, layer_ix):
    # Get the weights for the layer.
    layer = model.layers[layer_ix]
    
    if 'filters' not in layer.get_config().keys():
        return []
    W = layer.get_weights()[1]

    # Sum up all the weights for each filter.
    l1 = np.sum(np.abs(W), axis=(0,1,2))

    # Make list of (filter_ix, l1_norm), sorted by l1_norm (low to high).
    l1_norms = sorted(list(zip(range(len(l1)), l1)), key=lambda x: x[1])
    return l1_norms


def plot_l1_norms(model, layer_ix):
    p = list(map(lambda x: x[1], get_l1_norms(model, layer_ix)))
    if p==[]:
        print("nothing to plot")
        return
    plt.plot(p)
    plt.xlabel("Output channel", fontsize=18)
    plt.ylabel("L1 norm", fontsize=18)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.tick_params(axis='both', which='minor', labelsize=10)
    plt.title(model.layers[layer_ix].name, fontsize=18)



def load_conv_weights_from_dict(new_model, weights_dict):
    """Load the new weights into the changed layers (this is kinda slow)."""
    for ix in tqdm(range(len(weights_dict.keys()))):
        # print("Layer : ", new_model.layers[ix].name)
        new_model.layers[ix].set_weights(weights_dict[ix])
    

def prune_conv_layers(model, layer_ixs, percent_remove=0.05, mode="l1"):
    
    weights_dictionary={ix:model.layers[ix].get_weights() for ix in range(len(model.layers))}
    
    def delete_regular_conv_filters(layer_ix,channel_indices):
        layer = model.layers[layer_ix]
        channel_count = layer.get_config()["filters"] - len(channel_indices)
        
        pw_b_weights = [np.delete(w, channel_indices, axis=-1) for w in weights_dictionary[layer_ix][1:3]]

        weights_dictionary[layer_ix][1:3] = pw_b_weights

        # find the first following separable conv layer      
        temp_ix = layer_ix+1
        while (("conv" not in model.layers[temp_ix].get_config()['name']) and (temp_ix < len(model.layers)) ):
            temp_ix += 1

        if temp_ix == len(model.layers)-1:
            print("found the last layer!")
            return
        else:
            dw_weights = np.delete(model.layers[temp_ix].get_weights()[0], channel_indices, axis = 2)
            pw_weights = np.delete(model.layers[temp_ix].get_weights()[1], channel_indices, axis = 2) 
 
            weights_dictionary[temp_ix][0],weights_dictionary[temp_ix][1] = dw_weights,pw_weights
            print(weights_dictionary[temp_ix][1].shape)

        #print("Weight shapes after deletion: ", dw_weights.shape, pw_b_weights[0].shape, pw_b_weights[1].shape) 
        #layer.get_weights()[0].shape,layer.get_weights()[1].shape,layer.get_weights()[2].shape)
        layer.filters = channel_count

    # works only for depthwise seperable conv blocks 
    def delete_dw_conv_filters(layer_ix, channel_indices):
        """Remove output channels from a (pointwise) convolution layer that is 
        followed by a depthwise convolution and a pointwise convolution.
        """
        
        # Remove the output channels
        layer = model.layers[layer_ix]
        channel_count = layer.get_config()["filters"] - len(channel_indices)

        weights = [np.delete(w, channel_indices, axis=-1) for w in weights_dictionary[layer_ix]]

        # Remove the output channels from the BatchNorm layer as well
        BN_layer = model.layers[layer_ix + 1]
        BN_weights = [np.delete(w, channel_indices, axis=-1) for w in BN_layer.get_weights()]

        # Remove the input channels from the following depthwise conv layer
        dw_layer = model.layers[layer_ix + 3]
        dw_weights = [np.delete(dw_layer.get_weights()[0], channel_indices, axis=2),
                      np.delete(dw_layer.get_weights()[1], channel_indices)]
        
        # And from the next layer's BatchNorm layer as well
        dw_BN_layer = model.layers[layer_ix + 4]
        dw_BN_weights = [np.delete(w, channel_indices, axis=-1) for w in dw_BN_layer.get_weights()]
        
        # Remove the input channels from the next conv layer (no bias).
        pw_layer = model.layers[layer_ix + 5]
        pw_weights = [np.delete(pw_layer.get_weights()[0], channel_indices, axis=2), pw_layer.get_weights()[1]]
        #print("The shape of pw weights is: ", pw_weights[0].shape)
        
        pw_BN_layer = model.layers[layer_ix + 6]
        #print("The shape of pw weights is: ", pw_layer.get_weights()[0].shape)
        #pw_BN_weights = [np.delete(w, channel_indices, axis=-1) for w in pw_BN_layer.get_weights()]
        
        weights_dictionary[layer_ix] = weights
        weights_dictionary[layer_ix+1] = BN_weights
        weights_dictionary[layer_ix+3] = dw_weights
        weights_dictionary[layer_ix+4] = dw_BN_weights
        weights_dictionary[layer_ix+5] = pw_weights
        #weights_dictionary[layer_ix+6] = pw_BN_weights 
        
        # temp_ix = layer_ix+8
        
        # while ("conv" not in model.layers[temp_ix].name):
        #     temp_ix += 1
        # if("separable" in model.layers[temp_ix].name):
        #     return
        # #print(model.layers[temp_ix].name)
        # pw_weights = np.delete(model.layers[temp_ix].get_weights()[0], channel_indices, axis = 2) 
        # pw_bias = np.delete(model.layers[temp_ix].get_weights()[1], channel_indices)
        # weights_dictionary[temp_ix][0],weights_dictionary[temp_ix][1]  = pw_weights, pw_bias
        # print(weights_dictionary[temp_ix][0].shape)

        # Dirty trick: this changes the config for the current model, but that
        # does not seem to matter. This is what allows us to make a new model.
        layer.filters = channel_count

    for layer_ix in layer_ixs:
        num_original = model.layers[layer_ix].filters
        if mode=="l1":
            num_remove = int(model.layers[layer_ix].filters*percent_remove)
            l1_norms = get_l1_norms(model, layer_ix)
            
            # Get a sorted list of the filter indices to remove.
            l1_norms_pruned = l1_norms[:num_remove]
            channels_to_prune = sorted(list(map(lambda x: x[0], l1_norms_pruned)))
        elif mode=="apoz":
            validation_generator  = LoadBatches.distillationImageSegmentationGenerator( args.val_images , 
                                                args.val_annotations,args.val_batch_size,2, 130 , 
                                                384, 16, 48, annotation_filter=args.annotation_filter)
            apoz = get_apoz(model,model.layers[layer_ix].name,validation_generator)
            channels_to_prune = high_apoz(apoz)
            num_remove=len(channels_to_prune)
        
        print("Pruning %d of %d filters from layer %s" % (num_remove, num_original, model.layers[layer_ix].name))
        
        if args.model_type == 'xception' or args.model_type == 'shufflenet':
 
            delete_dw_conv_filters(layer_ix, channels_to_prune)
            # weights_dictionary[layer_ix] = w1
            # weights_dictionary[layer_ix+1] = w2
            # weights_dictionary[layer_ix+3] = w3
            # weights_dictionary[layer_ix+4] = w4
            # weights_dictionary[layer_ix+5] = w5
            # weights_dictionary[layer_ix+6] = w6

        elif args.model_type == 'vgg':
            delete_regular_conv_filters(layer_ix, channels_to_prune)
    new_model = model.__class__.from_config(model.get_config())
    
    # This is just so we can repeat the experiment more than once
    # model.layers[layer_ix].filters = num_original    
    
    # ACHTUNG! DOESN'T WORK FOR STUDENT MODELS
    new_model.outputHeight, new_model.outputWidth = model.layers[-3].output_shape[1], model.layers[-3].output_shape[2]

    load_conv_weights_from_dict(new_model, weights_dictionary)
    
    new_model.compile(loss="categorical_crossentropy",
                  optimizer="adadelta",
                  metrics=[metrics_and_losses.iou_score])

    print(new_model.summary())

    return new_model

random_id = "_id" + str(random.randint(0, 10000))
parent_folder = args.save_weights_path + '/pruning_' + args.model_type + random_id
val_ious = []
def retrain(m, pruning_iter, epochs=args.epochs//args.pruning_iterations , val_steps=(32/args.train_batch_size)*18 , train_steps=(32/args.train_batch_size)*100):
    
    train_generator  = LoadBatches.distillationImageSegmentationGenerator( args.train_images , 
                                             args.train_annotations ,args.train_batch_size,  2, 
                                             130 , 384 , m.outputHeight ,m.outputWidth,
                                             annotation_filter=args.annotation_filter)

    validation_generator  = LoadBatches.distillationImageSegmentationGenerator( args.val_images , 
                                              args.val_annotations,args.val_batch_size,2, 130 , 
                                              384, m.outputHeight , m.outputWidth,
                                              annotation_filter=args.annotation_filter)

    viou = m.evaluate_generator(validation_generator, steps=18)[1]
    val_ious.append(viou)
    print("Val_iou before any retraining: {:.3f}".format(viou))

    weights_path_name = parent_folder+'/'+str(pruning_iter)
    os.makedirs(weights_path_name) 

    filepath= weights_path_name + "/weights-improvement-{epoch:02d}-{val_iou_score:.4f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_iou_score', verbose=1, save_best_only=True, mode='max')
    tbcallback = TensorBoard(log_dir="logs/pruning/pruning_" + args.model_type + random_id + '/'+ str(pruning_iter),
                         histogram_freq=0, write_graph=True, write_images=True)
    
    m.fit_generator( train_generator ,train_steps , 
        validation_data=validation_generator , 
        validation_steps=val_steps ,  
        epochs=epochs,
        callbacks=[checkpoint,tbcallback])


model = load_model(args.model_file, custom_objects={
                                    'iou_score':metrics_and_losses.iou_score,
                                    'iou_score_student_hard':metrics_and_losses.iou_score_student_hard,})


layer_names = [layer.name for layer in model.layers]
layer_idxs = []

if args.model_type == 'xception': 

    # for i in range(1,3):
    #      for j in range(1,3):
    #         layer_idxs.append(layer_names.index("entry_flow_block"+str(i)+"_separable_conv"+str(j)+"_pointwise"))
    
    for i in range(1,9):
        for j in range(1,4):
            layer_idxs.append(layer_names.index("middle_flow_unit_"+str(i)+"_separable_conv"+str(j)+"_pointwise"))

elif args.model_type == 'vgg':

    for i in range(1,4):
        for j in range(1,i+1):
            layer_idxs.append(layer_names.index("block"+str(i)+"_conv"+str(j)))

    # for i in range(1,3):
    #     layer_idxs.append(layer_names.index("separable_conv2d_"+str(i)))

elif args.model_type == 'shufflenet':
    for i in range(2,4):
        for j in range(2,i+3):
            layer_idxs.append(layer_names.index("stage"+str(i)+"/block"+str(j)+"/1x1conv_1"))

def plot_val_ious(val_ious):
    plt.plot(val_ious)
    plt.xlabel("Pruning Iteration", fontsize=18)
    plt.ylabel("Val_iou", fontsize=18)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.tick_params(axis='both', which='minor', labelsize=10)
    plt.title("Val IoU", fontsize=18)
    plt.savefig(parent_folder+'/val_ious.png')

if __name__ == '__main__':
    # fig = plt.figure()
    # plot_l1_norms(model,layer_idxs[0])
    # plt.savefig("vgglayer1.png")
    pruned = model
    for pruning_iter in range(1,args.pruning_iterations):
        print("Pruning iteration: ", pruning_iter)
        pruned=prune_conv_layers(pruned,layer_idxs,mode=args.method)
        print("Pruning done, retrainig...")
        retrain(pruned,pruning_iter=pruning_iter)
    if args.plot_val_ious:
        plot_val_ious(val_ious)

