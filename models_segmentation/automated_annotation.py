from keras.models import Model, load_model
import argparse
import os
import metrics_and_losses
import cv2
import LoadBatches
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--images_dir", type = str, default = "../dataset_current/test")
parser.add_argument("--model_file", type = str, default = "../models_segmentation/selected_trained_models/xception-95.hdf5")
parser.add_argument("--save_auto", action = "store_true", default=False)
args = parser.parse_args()

img_path = './' + args.images_dir + '/'     #location of primary image folder
annotations_path = img_path + '../test_annotations'
rejects_path = img_path + '../test_rejected'
model = load_model(args.model_file, custom_objects={
                                    'iou_score':metrics_and_losses.iou_score,})

if not os.path.isdir(annotations_path):
    os.mkdir(annotations_path)

if not os.path.isdir(rejects_path):
    os.mkdir(rejects_path)

last_layer_shape = model.layers[-3].output_shape
output_width = last_layer_shape[2]
output_height = last_layer_shape[1]


for img in os.listdir(img_path):

    cv2_img = cv2.imread(img_path+'/'+img)
    img_array = LoadBatches.getImageArr(img_path+img, 384, 130)

    seg = model.predict(np.expand_dims(img_array, axis=0), batch_size=1).argmin(axis=-1)
    seg = seg.reshape(output_height, output_width)
    
    # lo.(
    seg_img = cv2.resize(seg , (384 , 130), interpolation=cv2.INTER_NEAREST)
    seg_img = np.stack((seg_img,)*3, axis=-1)*255
    
    img_toshow = (cv2_img*0.5 + seg_img*0.5).astype(np.uint8)
    
    winname = 'Xception Spoke So...'
    cv2.namedWindow(winname)        # Create a named window
    cv2.moveWindow(winname, 700,500) 
    cv2.imshow(winname, img_toshow)
    key = str(cv2.waitKey(0))
    
    # press a to add
    if key=='97':
        # save the annotation in the annotations folder
        cv2.imwrite(annotations_path +'/'+ img[:-3] + 'png', seg_img)

    # press d to not add (i want to say delete but then that's confusing)
    elif key=='100':
        # save the image itself in a seperate folder
        cv2.imwrite(rejects_path +'/'+ img, cv2_img)

    # press c to cancel  
    elif key=='99':
        break
    
    cv2.destroyAllWindows()

cv2.destroyAllWindows()