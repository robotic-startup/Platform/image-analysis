import tensorflow as tf
import keras
from keras import backend as K
from skimage.segmentation import find_boundaries
from sklearn.preprocessing import Binarizer
import numpy as np
import distance_vect_camera

def get_flops(model):
    run_meta = tf.RunMetadata()
    opts = tf.profiler.ProfileOptionBuilder.float_operation()

    # We use the Keras session graph in the call to the profiler.
    flops = tf.profiler.profile(graph=K.get_session().graph,
                                run_meta=run_meta, cmd='op', options=opts)

    return flops.total_float_ops  # Prints the "flops" of the model.

def iou_score(gt, pr):

    axes = [0, 1, 2]
        
    intersection = K.sum(gt * pr, axis=axes)
    union = K.sum(gt + pr, axis=axes) - intersection
    iou = (intersection + 1) / (union + 1)

    return iou

def iou_score_student_hard(gt, pr):
    #because output of student model is shape: [1, width*height, n_classes*2] we have to change it

    pr_hard = pr[:,:,:2] #use softmax without temperature vs with : pr[0,:,2:]
    gt_hard = gt[:,:,:2] #use hard ground_truth
    
    # pr_hard = K.argmax(pr_hard, axis=2)
    # gt_hard = K.argmax(gt_hard, axis=2)

    intersection = K.sum(gt_hard * pr_hard)
    union = K.sum(gt_hard + pr_hard) - intersection
    iou = (intersection + 1) / (union + 1)

    return iou

def iou_score_student_soft(gt, pr):
    #because output of student model is shape: [1, width*height, n_classes*2] we have to change it

    pr_soft = pr[:,:,2:] #use softmax with temperature vs without : pr[0,:,:2]
    gt_soft = gt[:,:,2:] #use soft ground_truth
    axes = [0, 1, 2]
        
    intersection = K.sum(gt_soft * pr_soft, axis=axes)
    union = K.sum(gt_soft + pr_soft, axis=axes) - intersection
    iou = (intersection + 1) / (union + 1)

    return iou

# def hard_iou_score(gt,pr):

#     # gt_hard=K.argmax(gt, axis=2)
#     # pr_hard=K.argmax(pr, axis=2) 
#     gt_hard = gt[:,:,:2]
#     pr_hard = pr[:,:,:2]
#     intersection = K.sum(gt_hard * pr_hard, axis=[0,1])
#     union = K.sum(gt_hard + pr_hard, axis=[0,1]) - intersection
#     iou = K.mean((intersection + 1) / (union + 1))

#     return iou

def weighted_iou_score(gt,pr):
    
    axes = [0, 1, 2]
        
    intersection = K.sum(gt * pr, axis=axes)
    union = K.sum(gt + pr, axis=axes) - intersection
    iou = (intersection + 1) / (union + 1)

    class_weights = K.mean(K.sum(gt,axis=[0,1])[::-1])
    iou = K.mean(iou * class_weights)

    return iou

def cross_entropy_with_class_weights(y_true,y_pred):

    frequencies = K.sum(y_true,axis=[0,1]) + 1
    loss = (-y_true)*K.log(y_pred)*frequencies[1] - (1-y_true) * K.log(K.abs(1-y_pred + K.epsilon()))*frequencies[0]
    return loss


def distilled_loss_teacher(temp):
    def loss_teacher(y_true, y_pred):  
        #val_loss = K.binary_crossentropy(y_true,y_pred)
        y_true_teacher = y_true[:,:,2:]
        y_pred_soft = y_pred[:,:,2:]
        return K.square(float(temp))*binary_log_loss()(y_true_teacher,y_pred_soft)  
       
        #return K.in_train_phase(train_loss,val_loss)
    return loss_teacher


def distilled_loss_gt():
    def loss_gt(y_true, y_pred): 
        y_true_gt = y_true[:,:,0:2]
        y_pred_notemp = y_pred[:,:,0:2]
        return binary_log_loss()(y_true_gt, y_pred_notemp)  
    return loss_gt

def distilled_loss_both(temp):
    def loss_both(y_true, y_pred):  

        y_true_gt = y_true[:,:,0:2]
        y_true_teacher = y_true[:,:,2:4]
        y_pred_soft = y_pred[:,:,2:4]
        y_pred_gt = y_pred[:,:,0:2]

        train_loss = K.square(float(temp))*binary_log_loss()(y_true_teacher,y_pred_soft) + binary_log_loss()(y_true_gt, y_pred_gt)  

        #return K.in_train_phase(train_loss,val_loss)

        train_loss = K.concatenate([train_loss, train_loss])

        return train_loss

    return loss_both




#custom loss function
def binary_log_loss_with_specifity(y_true, y_pred):
    neg_y_true = 1 - y_true
    neg_y_pred = 1 - y_pred
    FP = K.sum(neg_y_true * y_pred)
    TN = K.sum(neg_y_true * neg_y_pred)

    specifity = -K.log(TN / (TN + FP + K.epsilon()))

    logloss = -y_true*K.log(y_pred) - (neg_y_true) * K.log(K.abs(neg_y_pred + K.epsilon()))

    loss = args.specifity_parameter*specifity + logloss

    return loss
    

def binary_log_loss():
    def loss(y_true, y_pred):
        logloss = -(y_true*K.log(y_pred + K.epsilon()) + (1-y_true) * K.log(K.abs(1-y_pred + K.epsilon())))
        return logloss
    return loss

def binary_specificity_metric(y_true, y_pred):
    neg_y_true = 1 - y_true
    neg_y_pred = 1 - y_pred
    FP = K.sum(neg_y_true * y_pred)
    TN = K.sum(neg_y_true * neg_y_pred)
    specifity = -K.log(TN / (TN + FP + K.epsilon()))
    return specifity

def calc_fp_error(y_true, y_pred):
    neg_y_true = 1 - y_true
    error = neg_y_true * y_pred
    return error

def calc_fn_error(y_true, y_pred):
    neg_y_pred = 1 - y_pred    
    error = y_true * neg_y_pred
    return error

def binary_log_loss_metric(y_true, y_pred):
    eps = np.finfo(float).eps

    return -(y_true*np.log(y_pred + eps) + (1-y_true) * np.log(np.abs(1-y_pred + eps)))


def navrays_metric(rays_list):
    

    def calc_metric(y_true_2d, y_pred_2d):
        steepness_factor = 5
        max_ray_length = 18
        
        indexes_of_forward_rays = [0, 1, 2, 6, 7]
        
        #get vector distances
        distance_vect = distance_vect_camera.get_distance_vect(rays_list, y_pred_2d)
        #only pick forward facing rays with index 0, 1, 2, 6, 7
        vect_dist_pred = [distance_vect[i] for i in range(0, len(distance_vect)-1) if i in indexes_of_forward_rays]
        vect_dist_pred = np.array(vect_dist_pred)[:,0]
        # avoid_metric_pred = 1 / (vect_dist_pred + steepness_factor) * steepness_factor * (1-vect_dist_pred/max_ray_length)
        avoid_metric_pred = vect_dist_pred/max_ray_length
        distance_vect = distance_vect_camera.get_distance_vect(rays_list, y_true_2d)
        #only pick forward facing rays with index 0, 1, 2, 6, 7
        vect_dist_true = [distance_vect[i] for i in range(0, len(distance_vect)-1) if i in indexes_of_forward_rays]
        vect_dist_true = np.array(vect_dist_true)[:,0]

        # avoid_metric_true = 1 / (vect_dist_true + steepness_factor) * steepness_factor * (1-vect_dist_true/max_ray_length)
        avoid_metric_true= vect_dist_true/max_ray_length

        difference = np.abs(avoid_metric_pred - avoid_metric_true)
        return difference
    return calc_metric



''' Custom accuracy metric. Taken from https://hal.inria.fr/hal-01581525/document 
	A mix between global accuracy (Jaccard Index) and contour accuracy (BF Score, refer to the link above)
	Requires a distance threshold theta to compute contour accuracy
''' 
class BJMeasure(keras.callbacks.Callback):
	
	def __init__(self,val_data,theta=40):
		self.measure = []
		self.theta = theta
		self.validation_data = val_data

	def calculate_measure(self, y_true, y_pred):

		# threshold has to be greater than 0
		transformer = Binarizer(threshold = 0.1)
		TP = 0; FP = 0; FN = 0;
		for i in range(2):
			Sgt = transformer.fit_transform(y_true[:,:,i])
			Sps = transformer.fit_transform(y_pred[:,:,i])
			Bgt = find_boundaries(Sgt, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			Bps = find_boundaries(Sps, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

			TPgt = np.sum(np.maximum(0, 1 - np.square(np.linalg.norm(Bgt - Sps)/self.theta)))
			TPps = np.sum(np.maximum(0, 1 - np.square(np.linalg.norm(Bps - Sgt)/self.theta)))
			TP += TPgt + TPps
			FN += np.linalg.norm(Bgt, ord = 1) - TPgt
			FP += np.linalg.norm(Bps, ord = 1) - TPps
			
		# TP = tf.add(tf.cast(TPgt,tf.float64),TPps)
		# FN = tf.norm(tf.cast(Bgt,tf.float64), ord = 1) - tf.cast(TPgt,tf.float64)
		# FP = tf.norm(tf.cast(Bps,tf.float64), ord = 1) - TPps

		return (TP/2) / (TP/2 + FP/2 + FN/2)

	def on_epoch_end(self,epoch, logs={}):
		X,y_true = self.validation_data.next()
		y_pred = np.asarray(self.model.predict(X)).round()
 
		self.measure.append(self.calculate_measure(y_true, y_pred))
		#print "BJ measure: {}".format(self.measure[-1])
		return

