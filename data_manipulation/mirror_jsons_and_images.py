import glob
import json
import os
import os.path as osp
import argparse
import cv2
#use this script when you want to vertically flip any images or json shapes in given dir

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--in_dir')
args = parser.parse_args()

#mirror any json file in dir
#and delete image from json file because its not needed
for label_file in glob.glob(osp.join(args.in_dir, '*.json')):
    print('Rotating:', label_file)
    data = {}
    with open(label_file, "r") as f:
        data = json.load(f)
        shapes = data["shapes"]
        for area in shapes:
            for point in area["points"]:
                point[1] = (540 - (point[1] - 540))
        data['imageData']=None
    with open(label_file, "w") as f:
        json.dump(data, f, indent=4)


#mirror any images in dir
path = args.in_dir
os.chdir(path)
files = [f for f in os.listdir('.') if '.jpg' in f]

for f in files:

    img = cv2.imread(f)
    img = cv2.flip(img, 0)
    cv2.imwrite(f, img)
    print("mirrored ", f)