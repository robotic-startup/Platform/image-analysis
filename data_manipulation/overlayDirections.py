
import glob
import numpy as np
import cv2
import random
import argparse

def getDirection(seg):
	front_arrow = np.zeros_like( seg )
	right_arrow = np.zeros_like( seg )
	left_arrow = np.zeros_like( seg )
	cross = np.zeros_like( seg )
	for x in range(60, 180):
		for y in range(20, 80):
			if(abs(x - 120) <= y - 20 and y <= 50):
				front_arrow[y, x, :] = 255
			if(abs(x - 120) <= 10 and y > 50):
				front_arrow[y, x, :] = 255
			if(abs(y - 50) < x - 60 and x <= 90):
				left_arrow[y, x, :] = 255
			if(abs(y - 50) < 10 and x > 90):
				left_arrow[y, x, :] = 255
			if(abs(y - 50) < abs(x - 180) and x >= 150):
				right_arrow[y, x, :] = 255
			if(abs(y - 50) < 10 and x < 150):
				right_arrow[y, x, :] = 255
			if((abs(y - 50) < 10 and abs(x - 120) <= 30) or abs(x - 120) < 10):
				cross[y, x, :] = 255

	front_score = 0
	left_score = 0
	right_score = 0
	for x in range(70, 170):
		for y in range(80, 110):
			front_score += seg[y, x, 0]
	for y in range(80, 150):
		for x in range(30, 65):
			left_score += seg[y, x, 0]
		for x in range(180, 215):
			right_score += seg[y, x, 0]
	if(front_score > 3000*0.80):
		return front_arrow
	if(right_score > 2450*0.75):
		return right_arrow
	if(left_score > 2450*0.75):
		return left_arrow
	return cross


def imageSegmentationGenerator( segs_path ,  n_classes ):

	assert segs_path[-1] == '/'

	segmentations  = glob.glob( segs_path + "*.jpg"  ) + glob.glob( segs_path + "*.png"  ) +  glob.glob( segs_path + "*.jpeg"  )
	segmentations.sort()

	colors = [  ( random.randint(0,127),random.randint(0,127),random.randint(0,127)   ) for _ in range(n_classes)  ]


	for seg_fn in segmentations:
		seg = cv2.imread( seg_fn )
		print np.unique( seg )

		seg_img = np.zeros_like( seg )

		# for c in range(n_classes):
		# 	print("r: ", seg[:,:,0], " g: ", seg[:,:,1], " b: ", seg[:,:,2])
		overlay = getDirection(seg)
		for c in range(n_classes):
			seg_img[:,:,0] += (((seg[:,:,0] == c ) + (overlay[:,:,0] == 255))*( colors[c][0] )).astype('uint8')
			seg_img[:,:,1] += (((seg[:,:,0] == c ) + (overlay[:,:,0] == 255))*( colors[c][1] )).astype('uint8')
			seg_img[:,:,2] += (((seg[:,:,0] == c ) + (overlay[:,:,0] == 255))*( colors[c][2] )).astype('uint8')


		cv2.imshow("seg_img" , seg_img )
		cv2.waitKey()

parser = argparse.ArgumentParser()
parser.add_argument("--annotations", type = str  )
parser.add_argument("--n_classes", type=int )
args = parser.parse_args()


imageSegmentationGenerator(  args.annotations  ,  args.n_classes   )
