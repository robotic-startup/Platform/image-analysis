import cv2
import os

path = './segnet_images_test_grayscale'
for image_path in os.listdir(path):
    full_path = os.path.join(path, image_path)
    image = cv2.imread(full_path)



    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #print((image[0][0]))

    #cv2.imshow('gray_image',gray_image)
    #cv2.waitKey(0)                 # Waits forever for user to press any key
    #cv2.destroyAllWindows()
    cv2.imwrite(full_path,gray_image)
