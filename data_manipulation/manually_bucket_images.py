import os
import cv2
import shutil
import argparse

#this script will go through all files in img_path
#try to render them with cv2
#then move them into subfolders depending on what key you press for each image 
#press 0 for class0, 1 for class1 and q for quit

parser = argparse.ArgumentParser()
parser.add_argument("images_dir", 
            help="specify image folder to manually seperate")
args = parser.parse_args()

img_path = './' + args.images_dir + '/'     #location of primary image folder
c = 2           #number of different classes

files = os.listdir(img_path)
files.sort()
folder_names = []


for i in range(c):
    folder_names.append("data_class_" + str(i))
    if not os.path.exists(folder_names[i]):
        os.mkdir(folder_names[i])
    print(folder_names[i])

last_key = None
for f in files:
    img = cv2.imread(img_path + f)
    cv2.imshow("images", img)
    k = str(cv2.waitKey(0))
    if k=='113':        #q key
        break

    elif k == '48':     #0 key
        shutil.move(img_path + f, folder_names[0] + "/" + f)

    elif k == '49':     #1 key
        shutil.move(img_path + f, folder_names[1] + "/" + f)
    elif k == '255':     # delete key
        os.remove(img_path + f)
        print("deleted ", f)
    print(k, f)
