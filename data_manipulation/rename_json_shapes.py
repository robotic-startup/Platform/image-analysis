import os
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("--annotations_dir", 
            help="specify images folder containing json_files")
args = parser.parse_args()

files = os.listdir(args.annotations_dir)

jsons = [f  for f in files if f.endswith(".json")]
images = [f  for f in files if not f.endswith(".json")]
os.chdir(args.annotations_dir)


labels_to_delete=['brett']
labels_to_change_to_Rasen=['kurz', 'lang']

for i in jsons:
    #change image path file in json
    data = {}
    with open(i, "r") as f:
        data = json.load(f)
        shape_labels = []
        for l in data["shapes"]:
            shape_labels.append(l['label'])
        print(i, "shape labels are " , shape_labels)
        for l in range(len(shape_labels)):
            if shape_labels[l] in labels_to_change_to_Rasen:
                data["shapes"][l]["label"]="Rasen"
        for l in range(len(shape_labels)):        
            if shape_labels[l] in labels_to_delete:
                data["shapes"] = data["shapes"][:l] + data["shapes"][l+1:]
                
            
        shape_labels = []
        for l in data["shapes"]:
            shape_labels.append(l['label'])
        print("now labels are " , shape_labels)
        
    with open(i, "w") as f:
        json.dump(data, f, indent=4)
