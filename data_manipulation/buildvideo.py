import numpy as np
import cv2
import os

foldername = "videos_1_converted/"
scale_factor = 0.3
vidnames = os.listdir(foldername)
print(vidnames)
num_horizontal_images = 5
fps = 180.0
caps = []
for i in range(num_horizontal_images):
    caps.append(cv2.VideoCapture(foldername + vidnames[i]))

sizex,sizey = (int(1080 * scale_factor), int(1920 * scale_factor))

print("sizes: ", sizex,sizey)
padding = 10
combined_frame = np.zeros((sizey + padding, (sizex + padding)*num_horizontal_images, 3), np.uint8)
display_only = False

if not display_only:
    out = cv2.VideoWriter('output.mp4', cv2.VideoWriter_fourcc(*"DIV4"), fps, ((sizex+padding)*num_horizontal_images,(sizey+padding)))

counter = 0 
stop = False
while(stop==False):
    counter +=1
    frames = [[] for i in range(num_horizontal_images)]
    print("working on frame no. " +  str(counter))

    for i in range(num_horizontal_images):
        ret, frames[i] = caps[i].read()
        if(ret):
            #frames[i] = cv2.resize(frames[i], (sizex, sizey))
            frames[i] = cv2.copyMakeBorder(frames[i],padding,0,padding,0,cv2.BORDER_CONSTANT, value=(0,0,0))
            combined_frame[0 : sizey+padding, i*(sizex+padding) : (i+1)*(sizex+padding)] = frames[i]
        else:
            stop = True
        if not display_only:
            out.write(combined_frame)
        
    if display_only:
        cv2.imshow('frame',combined_frame)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    

# When everything done, release the capture
for i in range(num_horizontal_images):
    caps[i].release()
if not display_only:
    out.release()

cv2.destroyAllWindows()