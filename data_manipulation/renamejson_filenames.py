import glob
import json
import os
import os.path as osp
import argparse
parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--in_dir')
args = parser.parse_args()

for label_file in glob.glob(osp.join(args.in_dir, '*.json')):
        print('Rotating:', label_file)
        data = {}
        with open(label_file, "r") as f:
            data = json.load(f)
            data["imagePath"] = data["imagePath"].replace("picture", "image")
        with open(label_file, "w") as f:
            json.dump(data, f, indent=4)
