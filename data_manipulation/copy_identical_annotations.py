import os
import argparse
import shutil
import json

# this tool can be used when multiple images should be annotated the same way
# the folder passed in  --images_dir should contain a single json file and multiple images


parser = argparse.ArgumentParser()
parser.add_argument("--images_dir", 
            help="specify images folder containing a single json and mutliple images")
args = parser.parse_args()

files = os.listdir(args.images_dir)

jsons = [f  for f in files if f.endswith(".json")]
images = [f  for f in files if not f.endswith(".json")]

assert(len(jsons)==1)
assert(len(images)>0)

source_json = jsons[0]
os.chdir(args.images_dir)

for i in images:
    new_json =  i.rsplit('.', 1)[0] + '.json'
    if source_json != new_json:
        print("copied to " , new_json, source_json)
        shutil.copy(source_json, new_json) 
        
        #change image path file in json
        data = {}
        with open(new_json, "r") as f:
            data = json.load(f)
            data["imagePath"] = i
            data["imageData"] = None
        with open(new_json, "w") as f:
            json.dump(data, f, indent=4)
