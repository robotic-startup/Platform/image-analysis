'''
Using OpenCV takes a mp4 video and produces a number of images.

Requirements
----
You require OpenCV 3.2 to be installed.

Run
----
Open the main.py and edit the path to the video. Then run:
$ python main.py

Which will produce a folder called data with the images. There will be 2000+ images for example.mp4.
'''
import cv2
import numpy as np
import os

# Playing video from file:
datasetname = 'videos'
destinationname = 'data'

for foldername in os.listdir(datasetname):
    print('generating images in directory ' + foldername)
    for filename in os.listdir(datasetname + '/' + foldername):
        print('images in directory '  + filename)
        cap = cv2.VideoCapture(datasetname + '/' + foldername + '/' + filename)


        try:
            if not os.path.exists(destinationname + '/' + foldername):
                os.makedirs(destinationname +'/' + foldername)
        except OSError:
            print ('Error: Creating directory of data')

        currentFrame = 0
        number = 0
        while(True):
            # Capture frame-by-frame

            ret, frame = cap.read()
            if frame is None:
                break

            # Saves image of the current frame in jpg file
            name = './' + destinationname + '/' + foldername   + '/'+ filename + '_' + str(number) + '.jpg'
            print ('Creating...' + name)
            cv2.imwrite(name, frame)

            # To stop duplicate images
            currentFrame += 1
            number += 1
            for i in range(90):
                cap.grab();


        # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()

datasetname = 'videos'
destinationname = 'data'
