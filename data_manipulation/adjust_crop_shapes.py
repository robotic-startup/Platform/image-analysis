

import os
import shutil
import cv2
import matplotlib.pyplot as plt
import numpy as np

work_dir = "classifier_cropshapes_v2/"

#this method is used in several of the following steps
def recursive_search(current_dir, method):
    files = os.listdir(current_dir)
    files.sort()
    if all((".json" in mystring or ".jpg" in mystring or ".png" in mystring) for mystring in files):        
        method(current_dir)
        
    elif all((os.path.isdir(current_dir + mystring) or "classifier_shape.json" in mystring) for mystring in files):
        print("working in dir: " + current_dir)
        for folder in files:
            recursive_search(current_dir + "/" + folder + "/", method)
    else:
        print(current_dir, """dir must contain either only jpgs and 
              jsons or only dirs (and classifier_shape files). and end with /""")


def crop_images(current_dir):
        
    files_all = os.listdir(current_dir)
    files = [f for f in files_all if ".txt" not in f]
    i = 0


    y1 = 530
    y2 = 800
    x1 = 355
    x2 = 1510
    rotate = False
    firsttime = True

    while(True):
        img_orig = cv2.imread(current_dir + files[i])
        print("working on ", files[i])
        size = img_orig.shape
        img_vis = np.copy(img_orig)
        if(os.path.exists(current_dir + files[i] + ".txt") and firsttime == True):
            firsttime = False
            f = open(current_dir + files[i] + ".txt", "r")
            read_text = f.read()
            f.close()
            rlist = read_text.split(";")

            if(len(rlist)==5):

                x1 = int(rlist[0])
                x2 = int(rlist[1])
                y1 = int(rlist[2])
                y2 = int(rlist[3])
                rotate = bool(rlist[4].split(" ")[0])
            else:
                print("cant read txt file, wrong format")

        img_vis[y1:y2, x1:x2] = img_vis[y1:y2, x1:x2]/2
        img_crop = img_orig[y1:y2, x1:x2]
        # plt.subplot(211)
        # plt.imshow(img_vis)
        # plt.subplot(212)
        # plt.imshow(img_crop)
        # #plt.savefig("cropped_image_vis", transparent=False, dpi=400)
        # plt.show()
        cv2.namedWindow('vis',cv2.WINDOW_NORMAL)
        if(rotate):
            img_vis = cv2.flip( img_vis, 1 )
        cv2.imshow("vis", img_vis)
        
        keypressed = cv2.waitKey(0)
        keypressed = chr(keypressed)
        if(keypressed == 'q'):
            break
        elif(keypressed == 'w'):
            y1 -= 10
            y2 -= 10
            print(y2)
        elif(keypressed == 's'):
            y1 += 10
            y2 += 10
        elif(keypressed == 'a'):
            x1 -= 10
            x2 -= 10
        elif(keypressed == 'd'):
            x1 += 10
            x2 += 10
        elif(keypressed == 'u'):
            x1 -= 10
            x2 += 10
        elif(keypressed == 'j'):
            x1 += 10
            x2 -= 10
        elif(keypressed == 'i'):
            y1 -= 10
            y2 += 10
        elif(keypressed == 'k'):
            y1 += 10
            y2 -= 10
        elif(keypressed=="b"):
            print("skipping this image")
            firsttime = True
            i+= 1
        elif(keypressed=="r"):
            print("rotating")
            rotate = not rotate
        elif(keypressed=='n'):
            firsttime = True
            f= open(current_dir + files[i] + ".txt","w+")
            f.write(str(x1) + ";")
            f.write(str(x2) + ";")
            f.write(str(y1) + ";")
            f.write(str(y2) + ";")
            f.write(str(rotate))
            f.write(" meaning x1, x2, y1, y2, rotate")
            f.close()
            i+=1
        
        
            
    
recursive_search(work_dir, crop_images)

#260, 1560, 530, 900