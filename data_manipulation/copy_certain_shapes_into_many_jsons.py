import os
import argparse
import json


#use script when: you already annotated all images, and now want to add certain shapes into each of the json files(eg. the robot, which is always at the same place in the image) 
#step1 pick sample image and annotate with labelme
#step2 save the json file but check that the image and the json file have the same filename (otherwise labelme behaves weird when reopening the files) 
#step3 run script


parser = argparse.ArgumentParser()
parser.add_argument("--jsons_dir", required="True",
            help="specify annotations folder containing a single json and mutliple images")
parser.add_argument("--source_json", required="True",
            help="specify what json file top merge into all other jsons")
args = parser.parse_args()

files = os.listdir(args.jsons_dir)
jsons = [f  for f in files if f.endswith(".json")]

assert(os.path.isfile(args.source_json))
assert(len(jsons)>0)

##grab only the shapes from the annotation file
with open(args.source_json) as sj:
    data = json.load(sj)
    source_shapes = data["shapes"]
os.chdir(args.jsons_dir)

print("INFO: you may be adding the to be merged shapes more than once if you run the script multiple times because it is not checking for that")
#add source_json shapes to existing shapes of each file in jsons_dir
for j in jsons:
    
    data = {}
    with open(j, "r") as f:
        data = json.load(f)
        slen = len(source_shapes)
        data["shapes"] = data["shapes"] + source_shapes
    with open(j, "w") as f:
        json.dump(data, f, indent=4)
    print("copied shapes into ", j)

