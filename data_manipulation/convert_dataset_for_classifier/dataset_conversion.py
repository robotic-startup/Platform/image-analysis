import cv2
import os
import numpy as np
import shutil
import json
import matplotlib as mplt
import matplotlib.pyplot as plt


# ## usage
# this script will convert a image segmentation to a classification dataset
# it requires a foldername of the images and annotations respectively
# and requires a json file from labelme where the polygon was created
# the script will go through all annotations and check whether there are obstacles within that polygon.
# Then both image and annotation will be moved to target_dir/blocked or /free respectively
# 
# as the position of the robot is different in each subset of the dataset, an individual shape should be created for every subset.
# Otto created the datasets and still has the folder of seperate subsets

#TODO add capability of apllying this to a folder of dataset subsets which 
# each subset containing images, segmentations and an individual, single json shape


def run_conversion(foldername_images, foldername_annotations, shape_image):
    filenames_images = os.listdir(foldername_images)
    filenames_annotations = os.listdir(foldername_annotations)
    filenames_images.sort()
    filenames_annotations.sort()
    source_img = shape_image
    
    print("starting work on " + str(len(filenames_images)) + " images")
    for i in range(len(filenames_images)):
        # run through and check for obstacles    
        ann_img = cv2.imread(foldername_annotations + filenames_annotations[i])
        real_img = cv2.imread(foldername_images + filenames_images[i])

        #only use full hd images
        if(ann_img.shape[0]!=1080):
            print("""warning this folder contains images that are not full hd, 
                  this script doesnt support that, the non full hd images will be skipped""")
        else:
            ann_img = np.where(ann_img == 0, 1, 0)

            result_img = np.zeros((height, width))
            result_img = ann_img[:,:,0] * source_img
            obst_count = np.sum(result_img)

            ##visualize
            #plt.imshow(result_img)
            #plt.show()
            #plt.imshow(real_img)
            #plt.show()

            #copy image to respective new class
            if(obst_count>0):
                shutil.copy(foldername_images + filenames_images[i], target_dir + "blocked/images/" + filenames_images[i])
                shutil.copy(foldername_annotations + filenames_annotations[i], target_dir + "blocked/annotations/" + filenames_images[i])
            else:
                shutil.copy(foldername_images + filenames_images[i], target_dir + "free/images/" + filenames_images[i])
                shutil.copy(foldername_annotations + filenames_annotations[i], target_dir + "free/annotations/" + filenames_images[i])

    print("copied " + str(len(os.listdir(target_dir + "free/images/"))) + " image and annotation pairs to " + target_dir + "free")
    print("copied " + str(len(os.listdir(target_dir + "blocked/images/"))) + " image and annotation pairs to " + target_dir + "blocked")
    

def read_shape(height, width, json_file):
    
    #read shape from json, get pixel coordinates

    with open(json_file) as sj:
        data = json.load(sj)
        source_shapes = data["shapes"][0]['points']

    meshgrid = []
    for y in range(height):
        for x in range(width):
            meshgrid.append([])
            meshgrid[-1] = (x,y)

    overlays = np.array((width, height))

    p = source_shapes
    path_object = mplt.path.Path(p)

    shape_map = path_object.contains_points(meshgrid)
    shape_map = np.reshape(shape_map, (height, width))
    shape_map = np.where(shape_map==True, 1, 0)
    overlays = shape_map 

    return overlays


foldername_images = "data/data_fussballplatz_segnet/JPEGImages/"
foldername_annotations = "data/data_fussballplatz_segnet/segnet_data/"
target_dir = "data/classifier_dataset/"

#overwrite if already there
if os.path.exists(target_dir):
    shutil.rmtree(target_dir)
os.makedirs(target_dir)

os.mkdir(target_dir + "free/")
os.mkdir(target_dir + "free/images")
os.mkdir(target_dir + "free/annotations")
os.mkdir(target_dir + "blocked/")
os.mkdir(target_dir + "blocked/images")
os.mkdir(target_dir + "blocked/annotations")

json_file = "shapes/image.json"
height = 1080
width = 1920
source_img = read_shape(height, width, json_file)
run_conversion(foldername_images, foldername_annotations, source_img)