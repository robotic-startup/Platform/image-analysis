import os
import sys
import argparse
import cv2
from time import time
import pandas as pd
import datetime
from mobilenet_v2 import MobileNetv2
from keras.models import load_model
from mobilenet_v2 import * 

from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.layers import Conv2D, Reshape, Activation
from keras.models import Model
from keras.layers import * 
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()

parser.add_argument(
    "--model_file",
    help="The number of classes of dataset.",
    default="weights/training_run_2019-06-24 12:04:55.610256/weights-improvement-02-0.90.hdf5")
parser.add_argument(
    "--images_dir",
    help="folder with images to predict",
    default="data/validation"
)
batch = 1

args = parser.parse_args()
model_file_path = args.model_file
model = load_model(model_file_path, 
        custom_objects={'relu6':relu6, 'MobileNetV2': MobileNetv2})
last_layer_shape = model.layers[-3].output_shape
first_layer_shape = model.layers[0].output_shape
input_width = first_layer_shape[2]
input_height = first_layer_shape[1]
output_width = last_layer_shape[2]
output_height = last_layer_shape[1]

datagen2 = ImageDataGenerator(rescale=1. / 255)

validation_generator = datagen2.flow_from_directory(
    args.images_dir,
    target_size=(input_height, input_width),
    batch_size=batch,
    class_mode='categorical')

counter = 0

while True:

    x,y = next(validation_generator)
    #print(x.shape, y.shape)
    counter += 1
    pred_classwise = model.predict(x)
    pred = max(pred_classwise[0])
    text = "blocked"
    pred_class_index = 0
    pred_y = [1,0]

    if(pred==pred_classwise[0][1]):
        pred_class_index = 1
        text = "free"
        pred_y = [0,1]
    
    print(y[0], pred_y)
    pred_is_correct = (y[0] == pred_y)[0]
    print(str(pred_is_correct), " model predicted class " + text  + " with " + str(pred) + " certainty. True label class is " + str(y))
    

    cv2.imshow("prediction", x[0])
    keypressed = chr(cv2.waitKey(0))
    if(keypressed == "q"):
        break
    if(keypressed ==" "):
        pass
    