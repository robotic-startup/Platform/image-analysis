image analysis readme 
--------------

This repo should not include datasets. We store datasets in google drive

models_segmentation/ contains different versions of our segmentation model and training process and some tools

data_manipulation/ contains tools for the pipeline needed to create and manage the dataset

tools_segmentation/ contains tools for analyzing segmentation models or outputs 

classifier/ contains our work with classifiers. 