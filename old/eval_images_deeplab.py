import os
from io import BytesIO
import tarfile
import tempfile
from six.moves import urllib
import cv2
import time

from matplotlib import gridspec
from matplotlib import pyplot as plt
import numpy as np
import datetime
from PIL import Image

import tensorflow as tf

flags = tf.app.flags

FLAGS = flags.FLAGS

flags.DEFINE_string('inference_graph', 'frozen_inference_graph.pb', 'Where the inference graph resides.')
flags.DEFINE_string('eval_image', None, 'Path to single image to evaluate')
flags.DEFINE_string('eval_images_folder', None, 'Path to images folder to evaluate')

class DeepLabModel(object):
  """Class to load deeplab model and run inference."""

  INPUT_TENSOR_NAME = 'ImageTensor:0'
  OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
  INPUT_SIZE = 513
  FROZEN_GRAPH_NAME = 'frozen_inference_graph'

  def __init__(self):
    """Creates and loads pretrained deeplab model."""
    self.graph = tf.Graph()

    graph_def = None
    # Extract frozen graph from tar archive.
    file_handle = open(FLAGS.inference_graph, 'rb')
    graph_def = tf.GraphDef.FromString(file_handle.read())
    if graph_def is None:
      raise RuntimeError('Cannot find inference graph.')

    with self.graph.as_default():
      tf.import_graph_def(graph_def, name='')

    self.sess = tf.Session(graph=self.graph)

  def run(self, image):
    """Runs inference on a single image.

    Args:
      image: A PIL.Image object, raw input image.

    Returns:
      resized_image: RGB image resized from original input image.
      seg_map: Segmentation map of `resized_image`.
    """
    width, height = image.size
    resize_ratio = 1.0 * self.INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)
    batch_seg_map = self.sess.run(
        self.OUTPUT_TENSOR_NAME,
        feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
    seg_map = batch_seg_map[0]
    return resized_image, seg_map


def create_pascal_label_colormap():
  """Creates a label colormap used in PASCAL VOC segmentation benchmark.

  Returns:
    A Colormap for visualizing segmentation results.
  """
  colormap = np.zeros((256, 3), dtype=int)
  ind = np.arange(256, dtype=int)

  for shift in reversed(range(8)):
    for channel in range(3):
      colormap[:, channel] |= ((ind >> channel) & 1) << shift
    ind >>= 3

  return colormap


def label_to_color_image(label):
  """Adds color defined by the dataset colormap to the label.

  Args:
    label: A 2D array with integer type, storing the segmentation label.

  Returns:
    result: A 2D array with floating type. The element of the array
      is the color indexed by the corresponding element in the input label
      to the PASCAL color map.

  Raises:
    ValueError: If label is not of rank 2 or its value is larger than color
      map maximum entry.
  """
  if label.ndim != 2:
    raise ValueError('Expect 2-D input label')

  colormap = create_pascal_label_colormap()

  if np.max(label) >= len(colormap):
    raise ValueError('label value too large.')

  return colormap[label]


def vis_segmentation(image, seg_map, image_name, output_vis_dir):
  """Visualizes input image, segmentation map and overlay view."""
  plt.figure(figsize=(18, 10))
  grid_spec = gridspec.GridSpec(2, 2, width_ratios=[9,9])

  plt.subplot(grid_spec[0])
  plt.imshow(image)
  plt.axis('off')
  plt.title('input image')

  plt.subplot(grid_spec[1])
  seg_image = label_to_color_image(seg_map).astype(np.uint8)
  plt.imshow(seg_image)
  plt.axis('off')
  plt.title('segmentation map')

  plt.subplot(grid_spec[2])
  plt.imshow(image)
  plt.imshow(seg_image, alpha=0.7)
  plt.axis('off')
  plt.title('segmentation overlay')

  unique_labels = np.unique(seg_map)
  ax = plt.subplot(grid_spec[3])
  plt.imshow(
      FULL_COLOR_MAP[unique_labels].astype(np.uint8), interpolation='nearest')
  ax.yaxis.tick_right()
  plt.yticks(range(len(unique_labels)), LABEL_NAMES[unique_labels])
  plt.xticks([], [])
  ax.tick_params(width=0.0)
  plt.grid('off')

  plt.savefig(output_vis_dir + '/' + image_name)


LABEL_NAMES = np.asarray([
    'background', 'Rasen'
])

FULL_LABEL_MAP = np.arange(len(LABEL_NAMES)).reshape(len(LABEL_NAMES), 1)
FULL_COLOR_MAP = label_to_color_image(FULL_LABEL_MAP)

MODEL = DeepLabModel()
print('model loaded successfully!')


def run_visualization():
    print('running deeplab on image ')

    """Inferences DeepLab model and visualizes result."""
    if FLAGS.eval_images_folder is not None:
        images = []
        for f in os.listdir(FLAGS.eval_images_folder):
            if 'jpg' in f:
                images.append(f)
        if len(images) is 0:
            print('no .jpg images found')
        now = str(datetime.datetime.now())
        path_inference_dir = FLAGS.inference_graph.split('/')[0] + '/'
        output_dir = path_inference_dir + 'output_' + now
        os.mkdir(output_dir)
        output_vis_dir = path_inference_dir + 'visualization_' + now
        os.mkdir(output_vis_dir)

        segmentation_timer = []
        visualization_timer = []
        for image_name in images:
            f = open(FLAGS.eval_images_folder + "/" + image_name, 'rb')

            jpeg_str = f.read()
            original_im = Image.open(BytesIO(jpeg_str))

            tick = time.time()
            resized_im, seg_map = MODEL.run(original_im)
            seg_image = label_to_color_image(seg_map).astype(np.uint8)
            cv2.imwrite(output_dir + '/' + image_name, seg_image)
            tock = time.time()
            segmentation_timer.append(tock-tick)


            vis_segmentation(resized_im, seg_map, image_name, output_vis_dir)
            print('saved segmentation output of ' + image_name)

        total_time = 0
        for i in segmentation_timer:
            total_time += float(i)

        print('total segmentation duration for ' + str(len(images)) + ' images: ' +  str(total_time) + ' average: ' + str(total_time/len(images)))

    elif FLAGS.eval_image is not None:
        try:
            f = open(FLAGS.eval_image, 'rb')
            jpeg_str = f.read()
            original_im = Image.open(BytesIO(jpeg_str))
        except IOError:
            print('Cannot retrieve image. Please check path: ' + FLAGS.eval_image)
            resized_im, seg_map = MODEL.run(original_im)
            vis_segmentation(resized_im, seg_map, 'visualization.jpg')
    else:
        print("you have to specify path with --eval_image or --eval_images_folder")


run_visualization()
