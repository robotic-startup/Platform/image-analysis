import numpy as np
import cv2
import os

from io import BytesIO
import tarfile
import tempfile
from six.moves import urllib

from matplotlib import gridspec
from matplotlib import pyplot as plt
from PIL import Image

import tensorflow as tf

flags = tf.app.flags

FLAGS = flags.FLAGS

#This script tests trained deeplab models, by running them on the webcam input

flags.DEFINE_string('inference_graph', None, 'Where the inference graph resides.')
class DeepLabModel(object):
  """Class to load deeplab model and run inference."""

  INPUT_TENSOR_NAME = 'ImageTensor:0'
  OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
  INPUT_SIZE = 513
  FROZEN_GRAPH_NAME = 'frozen_inference_graph'

  def __init__(self):
    """Creates and loads pretrained deeplab model."""
    self.graph = tf.Graph()

    graph_def = None
    # Extract frozen graph from tar archive.
    file_handle = open(FLAGS.inference_graph)
    graph_def = tf.GraphDef.FromString(file_handle.read())
    if graph_def is None:
      raise RuntimeError('Cannot find inference graph.')

    with self.graph.as_default():
      tf.import_graph_def(graph_def, name='')

    self.sess = tf.Session(graph=self.graph)

  def run(self, image):
    """Runs inference on a single image.

    Args:
      image: A PIL.Image object, raw input image.

    Returns:
      resized_image: RGB image resized from original input image.
      seg_map: Segmentation map of `resized_image`.
    """
    width, height = image.size
    resize_ratio = 1.0 * self.INPUT_SIZE / max(width, height)
    target_size = (int(resize_ratio * width), int(resize_ratio * height))
    resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)
    batch_seg_map = self.sess.run(
        self.OUTPUT_TENSOR_NAME,
        feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})
    seg_map = batch_seg_map[0]
    return resized_image, seg_map


def create_pascal_label_colormap():
  """Creates a label colormap used in PASCAL VOC segmentation benchmark.

  Returns:
    A Colormap for visualizing segmentation results.
  """
  colormap = np.zeros((256, 3), dtype=int)
  ind = np.arange(256, dtype=int)

  for shift in reversed(range(8)):
    for channel in range(3):
      colormap[:, channel] |= ((ind >> channel) & 1) << shift
    ind >>= 3

  return colormap


def label_to_color_image(label):
  """Adds color defined by the dataset colormap to the label.

  Args:
    label: A 2D array with integer type, storing the segmentation label.

  Returns:
    result: A 2D array with floating type. The element of the array
      is the color indexed by the corresponding element in the input label
      to the PASCAL color map.

  Raises:
    ValueError: If label is not of rank 2 or its value is larger than color
      map maximum entry.
  """
  if label.ndim != 2:
    raise ValueError('Expect 2-D input label')

  colormap = create_pascal_label_colormap()

  if np.max(label) >= len(colormap):
    raise ValueError('label value too large.')

  return colormap[label]

LABEL_NAMES = np.asarray([
    'background', 'Rasen'
])

FULL_LABEL_MAP = np.arange(len(LABEL_NAMES)).reshape(len(LABEL_NAMES), 1)
FULL_COLOR_MAP = label_to_color_image(FULL_LABEL_MAP)

MODEL = DeepLabModel()
print('model loaded successfully!')

def run_visualization(img):
  """Inferences DeepLab model and visualizes result."""
  try:
    orignal_im = Image.fromarray(img)
  except IOError:
    print('Cannot retrieve image. Please check path: ' + FLAGS.eval_image)
    return

  resized_im, seg_map = MODEL.run(orignal_im)

  return resized_im, seg_map

#Setup the VideoCapture, to use the netcat Stream use "/dev/stdin", to use the Computer's standard webcam use 0
cap = cv2.VideoCapture(0)
#cap = cv2.VideoCapture("/dev/stdin")


while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Run the model on the image
    r, s = run_visualization(frame)
    resized_im = np.asarray(r)
    seg_map = np.asarray(s)
    seg_image = label_to_color_image(seg_map).astype(np.uint8)
    output = cv2.addWeighted(resized_im, 0.5, seg_image, 0.5,0)

    # Display the resulting frame
    cv2.imshow('frame', output)
    if cv2.waitKey(5) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
