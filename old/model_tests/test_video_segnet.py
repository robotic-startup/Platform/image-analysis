
import argparse
import Models , LoadBatches
from keras.models import load_model
import glob
import cv2
import numpy as np
import random

#This script tests trained segnet models, by running them on the webcam input

parser = argparse.ArgumentParser()
#The weights folder where segnet stores the trained model
parser.add_argument("--save_weights_path", type = str  )

#The input image's dimensions
parser.add_argument("--input_height", type=int , default = 224  )
parser.add_argument("--input_width", type=int , default = 224 )

#Model use for training segnet
parser.add_argument("--model_name", type = str , default = "vgg_segnet")

#number of classes to detect
parser.add_argument("--n_classes", type=int )

#epoch of the training to be used, enumeration starts at 0
parser.add_argument("--epoch_number", type=int )


args = parser.parse_args()

n_classes = args.n_classes
model_name = args.model_name
input_width =  args.input_width
input_height = args.input_height
epoch_number = args.epoch_number

modelFns = { 'vgg_segnet':Models.VGGSegnet.VGGSegnet , 'vgg_unet':Models.VGGUnet.VGGUnet , 'vgg_unet2':Models.VGGUnet.VGGUnet2 , 'fcn8':Models.FCN8.FCN8 , 'fcn32':Models.FCN32.FCN32   }
modelFN = modelFns[ model_name ]

m = modelFN( n_classes , input_height=input_height, input_width=input_width   )
m.load_weights(  args.save_weights_path + "." + str(epoch_number))
m.compile(loss='categorical_crossentropy',
      optimizer= 'adadelta' ,
      metrics=['accuracy'])
print('model loaded successfully')

output_height = m.outputHeight
output_width = m.outputWidth

colors = [  ( random.randint(0,255),random.randint(0,255),random.randint(0,255)   ) for _ in range(n_classes)  ]

def visImage(pr):
    #turn the raw seg_map into a colored image
    seg_img = np.zeros( ( output_height , output_width , 3  ) )
    for c in range(n_classes):
        seg_img[:,:,0] += ( (pr[:,: ] == c )*( colors[c][0] )).astype('uint8')
        seg_img[:,:,1] += ((pr[:,: ] == c )*( colors[c][1] )).astype('uint8')
        seg_img[:,:,2] += ((pr[:,: ] == c )*( colors[c][2] )).astype('uint8')
    seg_img = cv2.resize(seg_img  , (input_width , input_height ))
    return seg_img
#Setup the VideoCapture, to use the netcat Stream use "/dev/stdin", to use the Computer's standard webcam use 0
cap = cv2.VideoCapture(0)
#cap = cv2.VideoCapture("/dev/stdin")

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Run Segnet on the frame and visualize the ouput
    img = cv2.resize(frame, ( input_width , input_height ))
    img = img.astype(np.float32)
    img[:,:,0] -= 103.939
    img[:,:,1] -= 116.779
    img[:,:,2] -= 123.68
    img = np.rollaxis(img, 2, 0)
    pr = m.predict(np.array([img]))[0]
    pr = pr.reshape(( output_height ,  output_width , n_classes ) ).argmax( axis=2 )
    seg_img = visImage(pr)
    output = cv2.addWeighted(frame, 0.5, seg_img, 0.5,0)

    # Display the resulting frame
    cv2.imshow('frame', output)
    if cv2.waitKey(5) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
